#include "gr_mat_poly.h"

#include <flint/gr_mat.h>

int gr_mat_poly_mullow_poly(gr_poly_t result, gr_poly_t A, gr_poly_t f, slong len, gr_ctx_t mat_ctx)
{
    int status = GR_SUCCESS;

    gr_ctx_struct *coeff_ctx = MATRIX_CTX(mat_ctx)->base_ring;

    slong len_f = gr_poly_length(f, coeff_ctx);

    // TODO: Optimize this scalar multiplication.

    if (result == A) {
        gr_poly_t tmp_poly;
        gr_poly_init2(tmp_poly, len_f, mat_ctx);
        status |= gr_mat_poly_mullow_poly(tmp_poly, A, f, len, mat_ctx);
        gr_poly_swap(tmp_poly, result, mat_ctx);
        gr_poly_clear(tmp_poly, mat_ctx);
    } else {
        gr_poly_fit_length(result, len_f, mat_ctx);
        for (int i = 0; i < len_f; i++) {
            gr_ptr f_coeff = gr_poly_entry_ptr(f, i, coeff_ctx);
            gr_ptr result_coeff = gr_poly_entry_ptr(result, i, mat_ctx);
            status |= gr_mat_set_scalar(result_coeff, f_coeff, coeff_ctx);
        }
        _gr_poly_set_length(result, len_f, mat_ctx);

        status |= gr_poly_mullow(result, result, A, len, mat_ctx);
    }

    return status;
}

int gr_mat_poly_trace(gr_poly_t result, gr_poly_t poly, gr_ctx_t mat_ctx)
{
    int status = GR_SUCCESS;

    gr_ctx_struct *coeff_ctx = MATRIX_CTX(mat_ctx)->base_ring;

    gr_ptr tmp_scalar;
    GR_TMP_INIT(tmp_scalar, coeff_ctx);

    slong len = gr_poly_length(poly, mat_ctx);
    gr_poly_fit_length(result, len, coeff_ctx);

    for (slong i = 0; i < len; i++) {
        gr_ptr mat_ptr = gr_poly_entry_ptr(poly, i, mat_ctx);
        status |= gr_mat_trace(tmp_scalar, mat_ptr, coeff_ctx);
        status |= gr_poly_set_coeff_scalar(result, i, tmp_scalar, coeff_ctx);
    }

    GR_TMP_CLEAR(tmp_scalar, coeff_ctx);

    return status;
}

int gr_mat_poly_set_gr_poly_mat(gr_poly_t result, gr_mat_t A, gr_ctx_t poly_ctx, gr_ctx_t mat_ctx)
{
    int status = GR_SUCCESS;

    gr_ctx_struct *coeff_ctx = MATRIX_CTX(mat_ctx)->base_ring;

    slong len = 0;
    slong dim = gr_mat_nrows(A, coeff_ctx);
    for (slong i = 0; i < dim; i++) {
        for (slong j = 0; j < dim; j++) {
            slong entry_len = gr_poly_length(gr_mat_entry_ptr(A, i, j, poly_ctx), coeff_ctx);
            if (entry_len > len) {
                len = entry_len;
            }
        }
    }
    status |= gr_poly_zero(result, mat_ctx);
    gr_poly_fit_length(result, len, mat_ctx);
    for (slong i = 0; i < dim; i++) {
        for (slong j = 0; j < dim; j++) {
            gr_ptr A_entry = gr_mat_entry_ptr(A, i, j, poly_ctx);
            slong len2 = gr_poly_length(A_entry, coeff_ctx);
            for (slong k = 0; k < len2; k++) {
                gr_ptr A_entry_coeff = gr_poly_entry_ptr(A_entry, k, coeff_ctx);
                gr_ptr res_mat = gr_poly_entry_ptr(result, k, mat_ctx);
                gr_ptr res_mat_entry = gr_mat_entry_ptr(res_mat, i, j, coeff_ctx);
                status |= gr_set(res_mat_entry, A_entry_coeff, coeff_ctx);
            }
        }
    }
    _gr_poly_set_length(result, len, mat_ctx);

    return status;
}
