# d-finite-fun

Implementations of Newton iteration and divide-and-conquer to solve first-order systems of linear ordinary differential equations with polynomial coefficients.
These can also be used for higher-order scalar linear ordinary differential equations with polynomial coefficients.
