include("../BinarySplitting.jl")

println(:(1*2*3*4*5*6*7*8), " = ", @binary_splitting 1*2*3*4*5*6*7*8)
println(:(A1 * A2 * A3 * A4), " = ", @binary_splitting A1 * A2 * A3 * A4)
