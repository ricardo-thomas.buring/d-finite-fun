include("../DifferentialOperator.jl")
include("../FlintGenericRing.jl")

# TODO: Output as CSV.

const lib_d_finite_fun = joinpath(@__DIR__, "..", "libd-finite-fun_flint")
const SOLVE_ODE_METHOD_ASSUME_A_IS_COMPANION = 1

using Nemo

if length(ARGS) == 0
    println("Usage: julia ", @__FILE__, " <differential-operator-filenames>")
    exit()
end

# Helper functions to iterate over the integers (to find a suitable point at which to expand a power series).
function Base.iterate(ZZ::ZZRing)
    return zero(ZZ), one(ZZ)
end
function Base.iterate(::ZZRing, state)
    if state > 0
        return state, -state
    else
        return state, -(state - 1)
    end
end

# TODO: Adapt based on input.
order = 1024
C = AcbField(1024)
R, t = polynomial_ring(C, "t")
D_ring, D = polynomial_ring(R, "D")

files_amount = length(ARGS)
index_padding = length(string(files_amount))

for (index, filename) in enumerate(ARGS)
    print("[", string(index, pad=index_padding), " / ", files_amount, "] ")
    print(filename)
    f = open(filename)
    A_string = replace(read(f, String), "/" => "//", "\\" => "", "\n" => "")
    close(f)

    A_scalarform = eval(Meta.parse(A_string))

    diff_order = degree(A_scalarform)
    print(" of order ", diff_order)

    deg = maximum(degree(c) for c in coefficients(A_scalarform))
    print(" and degree ", deg)

    A_numerator, A_denominator = poly_mat_from_scalar_diffop(A_scalarform)

    # TODO: Use complex evaluation point.

    # Find shift.
    t0 = 0
    for k in ZZ
        if !contains_zero(evaluate(A_denominator, C(k)))
            t0 = k
            break
        end
    end

    println(" at t = ", t0, ": ")

    # Shift A_numerator and A_denominator.
    A_numerator = subst.(A_numerator, t + t0)
    A_denominator = subst(A_denominator, t + t0)

    Y0 = identity_matrix(C, size(A_numerator, 1))

    # Convert to FLINT.
    status = GR_SUCCESS

    A_coeff_ctx = gr_ctx_init_complex_acb(precision(C))
    A_poly_ctx = gr_ctx_init_gr_poly(A_coeff_ctx)

    # TODO: Make this configurable.
    sol_coeff_ctx = A_coeff_ctx
    sol_poly_ctx = A_poly_ctx

    A_numerator_flint = flint_gr_poly_mat_from_matrix(A_numerator, A_coeff_ctx, A_poly_ctx)
    A_denominator_flint = flint_gr_poly_from_polynomial(A_denominator, sol_coeff_ctx)
    method_flags = SOLVE_ODE_METHOD_ASSUME_A_IS_COMPANION
    Y0_flint = flint_gr_mat_from_matrix(Y0, sol_coeff_ctx)

    print("- Divide and conquer: ")

    Y_flint = gr_mat_init(size(A_numerator, 1), size(A_numerator, 1), sol_poly_ctx)

    benchmark_divideandconquer = @timed ccall((:gr_poly_mat_solve_ode_divide_and_conquer, lib_d_finite_fun), Cint,
        (Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}, Cint, Cint, Ptr{Cvoid}, Ptr{Cvoid}),
        Y_flint, A_numerator_flint, A_denominator_flint, Y0_flint, order, method_flags, A_poly_ctx, sol_poly_ctx)
    status |= benchmark_divideandconquer.value

    if status == GR_SUCCESS
        print(benchmark_divideandconquer.time, " / ")
        f_divideandconquer = gr_poly_init(sol_coeff_ctx)
        gr_mat_trace(f_divideandconquer, Y_flint, sol_poly_ctx)
        f_divideandconquer_squared = gr_poly_init(sol_coeff_ctx)
        benchmark_multiplication = @timed gr_poly_mul(f_divideandconquer_squared, f_divideandconquer, f_divideandconquer, sol_coeff_ctx)
        println(benchmark_multiplication.time, " = ", benchmark_divideandconquer.time / benchmark_multiplication.time)
        gr_poly_clear(f_divideandconquer_squared, sol_coeff_ctx)
        gr_poly_clear(f_divideandconquer, sol_coeff_ctx)
    else
        println("failed.")
    end

    gr_mat_clear(Y_flint, sol_poly_ctx)

    print("- Newton iteration: ")

    Y_flint = gr_mat_init(size(A_numerator, 1), size(A_numerator, 1), sol_poly_ctx)

    benchmark_newton = @timed ccall((:gr_poly_mat_solve_ode_newton, lib_d_finite_fun), Cint,
        (Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}, Cint, Cint, Ptr{Cvoid}, Ptr{Cvoid}),
        Y_flint, A_numerator_flint, A_denominator_flint, Y0_flint, order, method_flags, A_poly_ctx, sol_poly_ctx)
    status |= benchmark_newton.value

    if status == GR_SUCCESS
        print(benchmark_newton.time, " / ")
        f_newton = gr_poly_init(sol_coeff_ctx)
        gr_mat_trace(f_newton, Y_flint, sol_poly_ctx)
        f_newton_squared = gr_poly_init(sol_coeff_ctx)
        benchmark_multiplication = @timed gr_poly_mul(f_newton_squared, f_newton, f_newton, sol_coeff_ctx)
        println(benchmark_multiplication.time, " = ", benchmark_newton.time / benchmark_multiplication.time)
        gr_poly_clear(f_newton_squared, sol_coeff_ctx)
        gr_poly_clear(f_newton, sol_coeff_ctx)
    else
        println("failed.")
    end

    gr_mat_clear(Y_flint, sol_poly_ctx)

    println("- Newton / Divide and conquer = ", benchmark_newton.time / benchmark_divideandconquer.time)

    # Clean up.

    gr_mat_clear(Y0_flint, sol_coeff_ctx)
    gr_poly_clear(A_denominator_flint, sol_coeff_ctx)
    gr_mat_clear(A_numerator_flint, A_poly_ctx)
    if A_poly_ctx != sol_poly_ctx
        gr_ctx_clear(sol_poly_ctx)
        gr_ctx_clear(sol_coeff_ctx)
    end
    gr_ctx_clear(A_poly_ctx)
    gr_ctx_clear(A_coeff_ctx)
end
