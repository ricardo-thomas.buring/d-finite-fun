#include <stdio.h>
#include <string.h>
#include <time.h>

#include "../differential_operator_flint.h"
#include "../divide_and_conquer_flint.h"
#include "../newton_iteration_flint.h"
#include "../test_utils_flint.h"
#include "../gr_mat_poly.h"
#include "../solve_ode_method.h"

#include <flint/gr.h>
#include <flint/gr_poly.h>
#include <flint/gr_mat.h>
#include <flint/acb.h>

int main(int argc, char *argv[]) {
    // Parse command line arguments.
    int arg_idx = 1;
    int matrix_dops = 0;
    if (argc > 1 && strcmp(argv[1], "--matrix-dops") == 0) {
        matrix_dops = 1;
        arg_idx++;
    }
    if (argc == 1 || (argc == 2 && matrix_dops)) {
        printf("Usage: %s [--matrix-dops] <differential-operator-filenames>\n", argv[0]);
        return 1;
    }

    // TODO: Adapt based on input.
    slong len = 1024;
    slong prec = 4096;

    int status = GR_SUCCESS;

    gr_stream_t err;
    gr_stream_init_file(err, stderr);

    gr_ctx_t A_coeff_ctx;
    gr_ctx_init_complex_acb(A_coeff_ctx, prec);
    // gr_ctx_init_fmpq(A_coeff_ctx);

    gr_ctx_t A_poly_ctx;
    gr_ctx_init_gr_poly(A_poly_ctx, A_coeff_ctx);
    status |= gr_ctx_set_gen_name(A_poly_ctx, "t");

    gr_ctx_t dop_ctx;
    gr_ctx_init_gr_poly(dop_ctx, A_poly_ctx);
    status |= gr_ctx_set_gen_name(dop_ctx, "D");

    gr_poly_t A_scalarform;
    gr_poly_init(A_scalarform, A_poly_ctx);

    gr_mat_t A_numerator_poly_mat;
    gr_poly_t A_numerator_mat_poly;

    gr_poly_t A_denominator;
    gr_poly_init(A_denominator, A_coeff_ctx);

    // gr_ctx_t sol_coeff_ctx;
    // gr_ctx_init_complex_acb(sol_coeff_ctx, prec);
    // // gr_ctx_init_fmpq(sol_coeff_ctx);

    // gr_ctx_t sol_poly_ctx;
    // gr_ctx_init_gr_poly(sol_poly_ctx, sol_coeff_ctx);
    // status |= gr_ctx_set_gen_name(sol_poly_ctx, "t");

    // TODO: Make this configurable.
    gr_ctx_struct *sol_coeff_ctx = A_coeff_ctx;
    gr_ctx_struct *sol_poly_ctx = A_poly_ctx;

    flint_rand_t state;
    flint_rand_init(state);

    gr_ptr t0;
    GR_TMP_INIT(t0, A_coeff_ctx);
    gr_ptr A_denominator0;
    GR_TMP_INIT(A_denominator0, A_coeff_ctx);

    int files_amount = matrix_dops ? argc - 2 : argc - 1;

    printf("dop_filename,dop_order,dop_numerator_degree,dop_denominator_degree,dop_evpt");
    printf(",dac_poly_mat_elapsed_solving,dac_poly_mat_elapsed_squaring_trace,dac_poly_mat_radius_max_exp");
    printf(",dac_mat_poly_elapsed_solving,dac_mat_poly_elapsed_squaring_trace,dac_mat_poly_radius_max_exp");
    printf(",newton_poly_mat_elapsed_solving,newton_poly_mat_elapsed_squaring_trace,newton_poly_mat_radius_max_exp");
    printf(",newton_mat_poly_elapsed_solving,newton_mat_poly_elapsed_squaring_trace,newton_mat_poly_radius_max_exp");
    printf("\n");

    for (int idx = 1; idx <= files_amount; idx++, arg_idx++) {
        char *filename = argv[arg_idx];

        fprintf(stderr, "[ %03d / %03d ] %s", idx, files_amount, filename);

        int method_flags = 0;

        slong diff_order = -1;
        slong numerator_deg = -1;
        slong denominator_deg = -1;
        gr_ctx_t A_mat_ctx;
        gr_ctx_t sol_mat_ctx_tmp;
        gr_ctx_struct *sol_mat_ctx;
        gr_mat_t Y0;
        // TODO: Detect file type instead?
        if (matrix_dops) {
            FILE *fp = fopen(filename, "r");
            if (fp == NULL) {
                fprintf(stderr, ": error reading file\n");
                continue;
            }
            status |= gr_poly_mat_and_denom_load_file(A_numerator_poly_mat, A_denominator, fp, A_poly_ctx);
            fclose(fp);

            if (status == GR_UNABLE) {
                fprintf(stderr, ": error reading matrix differential operator from file\n");
                continue;
            }

            diff_order = gr_mat_nrows(A_numerator_poly_mat, sol_poly_ctx);
            for (slong i = 0; i < diff_order; i++) {
                for (slong j = 0; j < diff_order; j++) {
                    slong coeff_deg = gr_poly_length(gr_mat_entry_ptr(A_numerator_poly_mat, i, j, A_poly_ctx), A_coeff_ctx) - 1;
                    if (coeff_deg > numerator_deg) {
                        numerator_deg = coeff_deg;
                    }
                }
            }
            denominator_deg = gr_poly_length(A_denominator, A_coeff_ctx) - 1;
        } else {
            // Read file, ignoring newlines and backslashes.
            FILE *fp = fopen(filename, "r");
            if (fp == NULL) {
                fprintf(stderr, ": error reading file\n");
                continue;
            }
            char file_content[6969696];
            int character;
            int i = 0;
            while ((character = fgetc(fp)) != EOF && i < 6969696) {
                if (character == '\n' || character == '\\') {
                    continue;
                }
                file_content[i++] = character;
            }
            int read_whole_file = 0;
            if (i < 6969696) {
                file_content[i] = '\0';
                read_whole_file = 1;
            }
            fclose(fp);

            if (read_whole_file == 0) {
                fprintf(stderr, ": file too large to parse scalar differential operator from\n");
                continue;
            }

            status |= gr_set_str(A_scalarform, file_content, dop_ctx);

            if (status == GR_UNABLE) {
                fprintf(stderr, ": error parsing scalar differential operator from file\n");
                continue;
            }

            diff_order = gr_poly_length(A_scalarform, A_poly_ctx) - 1;
            numerator_deg = -1;
            for (slong k = 0; k <= diff_order; k++) {
                slong coeff_deg = gr_poly_length(gr_poly_entry_ptr(A_scalarform, k, A_poly_ctx), A_coeff_ctx) - 1;
                if (coeff_deg > numerator_deg) {
                    numerator_deg = coeff_deg;
                }
            }
            denominator_deg = gr_poly_length(gr_poly_entry_ptr(A_scalarform, diff_order, A_poly_ctx), A_coeff_ctx) - 1;

            gr_mat_init(A_numerator_poly_mat, diff_order, diff_order, A_poly_ctx);

            status |= gr_poly_mat_from_scalar_diffop(A_numerator_poly_mat, A_denominator, A_scalarform, A_poly_ctx);

            // TODO: Make this configurable.
            method_flags = SOLVE_ODE_METHOD_ASSUME_A_IS_COMPANION;
        }

        fprintf(stderr, " of order %ld, numerator degree %ld and denominator degree %ld", diff_order, numerator_deg, denominator_deg);

        if (gr_poly_is_zero(A_denominator, A_coeff_ctx) == T_TRUE) {
            fprintf(stderr, ": error, denominator vanishes identically\n");
            continue;
        }

        // TODO: Make initial conditions configurable.
        gr_mat_init(Y0, diff_order, diff_order, sol_coeff_ctx);
        status |= gr_mat_one(Y0, sol_coeff_ctx);

        gr_ctx_init_matrix_ring(A_mat_ctx, A_coeff_ctx, A_numerator_poly_mat->r);

        if (A_poly_ctx == sol_poly_ctx) {
            sol_mat_ctx = A_mat_ctx;
        } else {
            gr_ctx_init_matrix_ring(sol_mat_ctx_tmp, sol_coeff_ctx, A_numerator_poly_mat->r);
            sol_mat_ctx = sol_mat_ctx_tmp;
        }

        gr_poly_init(A_numerator_mat_poly, A_mat_ctx);
        status |= gr_mat_poly_set_gr_poly_mat(A_numerator_mat_poly, A_numerator_poly_mat, A_poly_ctx, A_mat_ctx);

        // Find evaluation point.
        while (1) {
            // TODO: Better random generation in other contexts.
            if (A_coeff_ctx->which_ring == GR_CTX_CC_ACB) {
                acb_urandom(t0, state, prec);
            } else {
                status |= gr_randtest(t0, state, A_coeff_ctx);
            }
            status |= gr_poly_evaluate(A_denominator0, A_denominator, t0, A_coeff_ctx);
            if (gr_is_zero(A_denominator0, A_coeff_ctx) == T_FALSE) {
                break;
            }
        }

        fprintf(stderr, " at t = ");
        if (A_coeff_ctx->which_ring == GR_CTX_CC_ACB) {
            // NOTE: We assume the evaluation point is exact.
            arb_struct *real_part = acb_realref((acb_struct *)t0);
            arf_fprint(stderr, &real_part->mid);
            fprintf(stderr, " + ");
            arb_struct *imag_part = acb_imagref((acb_struct *)t0);
            arf_fprint(stderr, &imag_part->mid);
            fprintf(stderr, "*I");
        } else {
            gr_write(err, t0, A_coeff_ctx);
        }
        fprintf(stderr, "\n");

        // CSV output.
        printf("%s,%ld,%ld,%ld", filename, diff_order, numerator_deg, denominator_deg);
        printf(",\"");
        if (A_coeff_ctx->which_ring == GR_CTX_CC_ACB) {
            // NOTE: We assume the evaluation point is exact.
            arb_struct *real_part = acb_realref((acb_struct *)t0);
            arf_print(&real_part->mid);
            printf(" + ");
            arb_struct *imag_part = acb_imagref((acb_struct *)t0);
            arf_print(&imag_part->mid);
            printf("*I");
        } else {
            gr_print(t0, A_coeff_ctx);
        }
        printf("\"");
        fflush(stdout);

        int test_flags = SOLVE_ODE_TEST_WRITE_PROGRESS;

        solve_ode_test_result_t result_dac_poly_mat;
        status |= test_gr_poly_mat_solve_ode(result_dac_poly_mat, test_flags, err, A_numerator_poly_mat, A_denominator, t0, Y0, len, &gr_poly_mat_solve_ode_divide_and_conquer, "Divide and conquer (matrices with polynomial entries)", method_flags, A_poly_ctx, sol_poly_ctx);
        printf(",%f,%f,", result_dac_poly_mat->time_elapsed_solving, result_dac_poly_mat->time_elapsed_squaring_trace);
        fmpz_print(&result_dac_poly_mat->radius_max_exp);

        solve_ode_test_result_t result_dac_mat_poly;
        status |= test_gr_mat_poly_solve_ode(result_dac_mat_poly, test_flags, err, A_numerator_mat_poly, A_denominator, t0, Y0, len, &gr_mat_poly_solve_ode_divide_and_conquer, "Divide and conquer (polynomials with matrix coefficients)", method_flags, A_mat_ctx, sol_mat_ctx);
        printf(",%f,%f,", result_dac_mat_poly->time_elapsed_solving, result_dac_mat_poly->time_elapsed_squaring_trace);
        fmpz_print(&result_dac_mat_poly->radius_max_exp);

        solve_ode_test_result_t result_newton_poly_mat;
        status |= test_gr_poly_mat_solve_ode(result_newton_poly_mat, test_flags, err, A_numerator_poly_mat, A_denominator, t0, Y0, len, &gr_poly_mat_solve_ode_newton, "Newton iteration (matrices with polynomial entries)", method_flags, A_poly_ctx, sol_poly_ctx);
        printf(",%f,%f,", result_newton_poly_mat->time_elapsed_solving, result_newton_poly_mat->time_elapsed_squaring_trace);
        fmpz_print(&result_newton_poly_mat->radius_max_exp);

        solve_ode_test_result_t result_newton_mat_poly;
        status |= test_gr_mat_poly_solve_ode(result_newton_mat_poly, test_flags, err, A_numerator_mat_poly, A_denominator, t0, Y0, len, &gr_mat_poly_solve_ode_newton, "Newton iteration (polynomials with matrix coefficients)", method_flags, A_mat_ctx, sol_mat_ctx);
        printf(",%f,%f,", result_newton_mat_poly->time_elapsed_solving, result_newton_mat_poly->time_elapsed_squaring_trace);
        fmpz_print(&result_newton_mat_poly->radius_max_exp);

        printf("\n");
        fflush(stdout);

        gr_poly_clear(A_numerator_mat_poly, A_mat_ctx);
        if (sol_mat_ctx != A_mat_ctx) {
            gr_ctx_clear(sol_mat_ctx_tmp);
        }
        gr_mat_clear(A_numerator_poly_mat, A_poly_ctx);
        gr_ctx_clear(A_mat_ctx);
        gr_mat_clear(Y0, sol_coeff_ctx);
    }

    GR_TMP_CLEAR(A_denominator0, A_coeff_ctx);
    GR_TMP_CLEAR(t0, A_coeff_ctx);
    flint_rand_clear(state);
    if (A_poly_ctx != sol_poly_ctx) {
        gr_ctx_clear(sol_poly_ctx);
        gr_ctx_clear(sol_coeff_ctx);
    }
    gr_poly_clear(A_denominator, A_coeff_ctx);
    gr_poly_clear(A_scalarform, A_poly_ctx);
    gr_ctx_clear(dop_ctx);
    gr_ctx_clear(A_poly_ctx);
    gr_ctx_clear(A_coeff_ctx);

    flint_cleanup_master();

    return 0;
}
