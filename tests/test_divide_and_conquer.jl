include("../DivideAndConquer.jl")
include("../DifferentialOperator.jl")
include("../test_utils.jl")

using Nemo

order = 8
C = AcbField(64)
R, t = polynomial_ring(C, "t")
D, Dt = polynomial_ring(R, "Dt")

A_scalarform = t^3*(32*t^2-1)*(32*t^2+1)*Dt^4+2*t^2*(7168*t^4-3)*Dt^3+t*(55296*t^4-7)*Dt^2+(61440*t^4-1)*Dt+12288*t^3
t0 = 1

test_poly_mat_solve_ode(A_scalarform, t0, order, poly_mat_solve_ode_divide_and_conquer_preinv, "Divide and conquer (matrices with polynomial entries, pre-computed denominator inverse)")
println()
test_poly_mat_solve_ode(A_scalarform, t0, order, poly_mat_solve_ode_divide_and_conquer, "Divide and conquer (matrices with polynomial entries, iteration for denominator inverse)")
println()
test_mat_poly_solve_ode(A_scalarform, t0, order, mat_poly_solve_ode_divide_and_conquer_preinv, "Divide and conquer (polynomials with matrix coefficients, pre-computed denominator inverse)")
println()
test_mat_poly_solve_ode(A_scalarform, t0, order, mat_poly_solve_ode_divide_and_conquer, "Divide and conquer (polynomials with matrix coefficients, iteration for denominator inverse)")
