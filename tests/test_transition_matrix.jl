include("../NewtonIteration.jl")
include("../DifferentialOperator.jl")

using Nemo

order = 64
precision = 128
C = AcbField(precision)
R, t = polynomial_ring(C, "t")
D, Dt = polynomial_ring(R, "Dt")

A_scalarform = t*Dt^2 + Dt

A_numerator, A_denominator = poly_mat_from_scalar_diffop(A_scalarform)

function transition_matrix(A_numerator, A_denominator, z0, z1, order)
    R = base_ring(A_numerator)
    z = gen(R)
    C = base_ring(R)
    A_numerator_shifted = subst.(A_numerator, z + z0)
    A_denominator_shifted = subst(A_denominator, z + z0)
    Y0 = identity_matrix(C, size(A_numerator, 1))
    Y = poly_mat_solve_ode_newton(A_numerator_shifted, A_denominator_shifted, Y0, order)

    # NOTE: Divide by factorials here if Y = (y, y', y'') is defined without division by factorials.
    for i in 1:size(Y, 1)
        for j in 1:size(Y, 1)
            Y[i,j] /= factorial(i-1)
        end
    end

    Y1 = evaluate.(Y, z1 - z0)
    return Y1
end

invsqrt2 = 1/sqrt(C(2))
i = C(0, 1)
Y1 = transition_matrix(A_numerator, A_denominator, C(-1, 0), -invsqrt2 + i*invsqrt2, order)
Y2 = transition_matrix(A_numerator, A_denominator, -invsqrt2 + i*invsqrt2, C(0, 1), order)
Y3 = transition_matrix(A_numerator, A_denominator, C(0, 1), invsqrt2 + i*invsqrt2, order)
Y4 = transition_matrix(A_numerator, A_denominator, invsqrt2 + i*invsqrt2, C(1,0), order)
Y5 = transition_matrix(A_numerator, A_denominator, C(1,0), invsqrt2 - i*invsqrt2, order)
Y6 = transition_matrix(A_numerator, A_denominator, invsqrt2 - i*invsqrt2, C(0, -1), order)
Y7 = transition_matrix(A_numerator, A_denominator, C(0, -1), -invsqrt2 - i*invsqrt2, order)
Y8 = transition_matrix(A_numerator, A_denominator, -invsqrt2 - i*invsqrt2, C(-1, 0), order)

println("Y8*Y7*Y6*Y5*Y4*Y3*Y2*Y1 = ", Y8*Y7*Y6*Y5*Y4*Y3*Y2*Y1)
