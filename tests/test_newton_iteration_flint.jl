include("../DifferentialOperator.jl")
include("../newton_iteration_flint.jl")

using Nemo

const SOLVE_ODE_METHOD_ASSUME_A_IS_COMPANION = 1

order = 8
C = AcbField(64)
R, t = polynomial_ring(C, "t")
D, Dt = polynomial_ring(R, "Dt")

A_scalarform = t^3*(32*t^2-1)*(32*t^2+1)*Dt^4+2*t^2*(7168*t^4-3)*Dt^3+t*(55296*t^4-7)*Dt^2+(61440*t^4-1)*Dt+12288*t^3

A_numerator, A_denominator = poly_mat_from_scalar_diffop(A_scalarform)

# Shift A_numerator and A_denominator.
t0 = 1
A_numerator = subst.(A_numerator, t + t0)
A_denominator = subst(A_denominator, t + t0)
method_flags = SOLVE_ODE_METHOD_ASSUME_A_IS_COMPANION

Y0 = identity_matrix(C, size(A_numerator, 1))
status, Y = poly_mat_solve_ode_newton_flint(A_numerator, A_denominator, Y0, order, method_flags)
println("Y = ", Y, "\n")

# TODO: Test by computing (A_denominator*Y' - A_numerator*Y) mod t^{order - 1}.

if status == GR_SUCCESS
    println("Newton iteration succeeded.")
else
    println("Newton iteration failed.")
end
