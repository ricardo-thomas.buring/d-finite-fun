#include <stdio.h>
#include "../differential_operator_flint.h"
#include "../newton_iteration_flint.h"
#include "../test_utils_flint.h"
#include "../solve_ode_method.h"

// To detect acb context.
#include <flint/gr.h>
#include <flint/fmpz.h>

int main()
{
    slong len = 8;
    int status = GR_SUCCESS;

    gr_stream_t out;
    gr_stream_init_file(out, stdout);

    gr_ctx_t A_coeff_ctx;
    gr_ctx_init_complex_acb(A_coeff_ctx, 64);
    // gr_ctx_init_fmpq(A_coeff_ctx);

    gr_ctx_t A_poly_ctx;
    gr_ctx_init_gr_poly(A_poly_ctx, A_coeff_ctx);
    status |= gr_ctx_set_gen_name(A_poly_ctx, "t");

    gr_ctx_t dop_ctx;
    gr_ctx_init_gr_poly(dop_ctx, A_poly_ctx);
    status |= gr_ctx_set_gen_name(dop_ctx, "D");

    gr_poly_t A_scalarform;
    gr_poly_init(A_scalarform, A_poly_ctx);
    status |= gr_set_str(A_scalarform, "t^3*(32*t^2-1)*(32*t^2+1)*D^4+2*t^2*(7168*t^4-3)*D^3+t*(55296*t^4-7)*D^2+(61440*t^4-1)*D+12288*t^3", dop_ctx);

    // If Y'(t) = A(t)*Y(t) then Y2(t) = Y(t + t0) satisfies Y2'(t) = A(t + t0)*Y2(t).
    // Choose t0 = 1.
    gr_ptr t0;
    GR_TMP_INIT(t0, A_coeff_ctx);
    status |= gr_set_si(t0, 1, A_coeff_ctx);

    // gr_ctx_t sol_coeff_ctx;
    // gr_ctx_init_complex_acb(sol_coeff_ctx, 64);
    // // gr_ctx_init_fmpq(sol_coeff_ctx);

    // gr_ctx_t sol_poly_ctx;
    // gr_ctx_init_gr_poly(sol_poly_ctx, sol_coeff_ctx);
    // status |= gr_ctx_set_gen_name(sol_poly_ctx, "t");

    gr_ctx_struct *sol_coeff_ctx = A_coeff_ctx;
    gr_ctx_struct *sol_poly_ctx = A_poly_ctx;

    // Y0 = identity_matrix(R, 4)
    slong diff_order = gr_poly_length(A_scalarform, A_poly_ctx) - 1;
    gr_mat_t Y0;
    gr_mat_init(Y0, diff_order, diff_order, sol_coeff_ctx);
    status |= gr_mat_one(Y0, sol_coeff_ctx);

    solve_ode_test_result_t test_result;
    int test_flags = SOLVE_ODE_TEST_WRITE_PROGRESS | SOLVE_ODE_TEST_WRITE_SOLUTION;

    for (int method_flags = 0; method_flags < SOLVE_ODE_METHOD_FLAG_MAX; method_flags++) {
        status |= test_gr_poly_mat_solve_scalar_ode(test_result, test_flags, out, A_scalarform, t0, Y0, len, &gr_poly_mat_solve_ode_newton, "Newton iteration (matrices with polynomial entries)", method_flags, A_poly_ctx, sol_poly_ctx);
        gr_stream_write(out, "\n");
    }

    for (int method_flags = 0; method_flags < SOLVE_ODE_METHOD_FLAG_MAX; method_flags++) {
        status |= test_gr_mat_poly_solve_scalar_ode(test_result, test_flags, out, A_scalarform, t0, Y0, len, &gr_mat_poly_solve_ode_newton, "Newton iteration (polynomials with matrix coefficients)", method_flags, A_poly_ctx, sol_poly_ctx);
        gr_stream_write(out, "\n");
    }

    gr_mat_clear(Y0, sol_coeff_ctx);
    // gr_ctx_clear(sol_poly_ctx);
    // gr_ctx_clear(sol_coeff_ctx);
    GR_TMP_CLEAR(t0, A_coeff_ctx);
    gr_poly_clear(A_scalarform, A_poly_ctx);
    gr_ctx_clear(dop_ctx);
    gr_ctx_clear(A_poly_ctx);
    gr_ctx_clear(A_coeff_ctx);

    flint_cleanup_master();

    return 0;
}
