using Nemo
using ArgParse
using Random

function parse_arguments()
    settings = ArgParseSettings()

    @add_arg_table! settings begin
        "--min-order"
            help = "the minimum order"
            arg_type = Int
            default = 0
        "--max-order"
            help = "the maximum order"
            arg_type = Int
            default = 0
        "--min-numerator-degree"
            help = "the minimum numerator degree"
            arg_type = Int
            default = 0
        "--max-numerator-degree"
            help = "the maximum numerator degree"
            arg_type = Int
            default = 0
        "--min-denominator-degree"
            help = "the minimum denominator degree"
            arg_type = Int
            default = 0
        "--max-denominator-degree"
            help = "the maximum denominator degree"
            arg_type = Int
            default = 0
        "--min-coefficient"
            help = "the minimum coefficient of a generated factor"
            arg_type = Int
            default = -100
        "--max-coefficient"
            help = "the maximum coefficient of a generated factor"
            arg_type = Int
            default = 100
        "--min-density"
            help = "the minimum density of the matrix"
            arg_type = Float64
            default = 0.0
        "--max-density"
            help = "the maximum density of the matrix"
            arg_type = Float64
            default = 1.0
        "--sample-size"
            help = "the number of polynomial matrix ODE's to generate"
            arg_type = Int
            default = 1
    end

    return parse_args(settings)
end


function save_poly_mat_ode(A_numerator, A_denominator, filename)
    f = open(filename, "w")
    order = size(A_numerator, 1)
    write(f, "$order $order fmpq_poly\n")
    for i in 1:order
        for j in 1:order
            if A_numerator[i,j] == 0
                continue
            end
            A_entry_str = replace("$(A_numerator[i,j])", "//" => "/")
            write(f, "$(i-1) $(j-1) $(A_entry_str)\n")
        end
    end
    write(f, "0 0 0\n")
    A_denominator_str = replace("$(A_denominator)", "//" => "/")
    write(f, "$(A_denominator_str)\n")
    close(f)
end


function random_polynomial_product(R, total_degree, factor_min_coefficient, factor_max_coefficient)
    unit = rand(R, 0:0, factor_min_coefficient:factor_max_coefficient)
    result = unit
    while degree(result) < total_degree
        random_degree = rand(1:(total_degree - degree(result)))
        factor = rand(R, random_degree:random_degree, factor_min_coefficient:factor_max_coefficient)
        result *= factor
    end
    @assert degree(result) == total_degree
    return result
end


function random_poly_mat_ode(R, min_order, max_order, min_numerator_degree, max_numerator_degree, min_denominator_degree, max_denominator_degree, min_coefficient, max_coefficient, min_density, max_density)
    order = rand(min_order:max_order)
    A_numerator = zero_matrix(R, order, order)
    # Choose the number of nonzero entries.
    num_matrix_entries = order*order
    num_nonzeros = rand(ceil(Int, min_density*num_matrix_entries):floor(Int, max_density*num_matrix_entries))
    # Choose that number of different indexes.
    random_indexes = [i - 1 for i in Iterators.take(randperm(order*order), num_nonzeros)]
    nonzero_entry_indexes = [CartesianIndex(div(i, order) + 1, (i % order) + 1) for i in random_indexes]
    # Assign random polynomials to entries at those indexes.
    for I in nonzero_entry_indexes
        degree = rand(min_numerator_degree:max_numerator_degree)
        A_numerator[I] = random_polynomial_product(R, degree, min_coefficient, max_coefficient)
    end
    A_denominator_degree = rand(min_denominator_degree:max_denominator_degree)
    A_denominator = random_polynomial_product(R, A_denominator_degree, min_coefficient, max_coefficient)
    return A_numerator, A_denominator
end


function main(ARGS)
    args = parse_arguments()

    min_order = args["min-order"]
    max_order = args["max-order"]
    min_numerator_degree = args["min-numerator-degree"]
    max_numerator_degree = args["max-numerator-degree"]
    min_denominator_degree = args["min-denominator-degree"]
    max_denominator_degree = args["max-denominator-degree"]
    min_coefficient = args["min-coefficient"]
    max_coefficient = args["max-coefficient"]
    min_density = args["min-density"]
    max_density = args["max-density"]
    sample_size = args["sample-size"]

    # TODO: Check ranges etc.

    R, t = polynomial_ring(QQ, :t)

    # TODO: Deterministically, for all total-degree sequences, for all degree-sequences of factorizations...

    for counter in 1:sample_size
        A_numerator, A_denominator = random_poly_mat_ode(R, min_order, max_order, min_numerator_degree, max_numerator_degree, min_denominator_degree, max_denominator_degree, min_coefficient, max_coefficient, min_density, max_density)
        save_poly_mat_ode(A_numerator, A_denominator, "dop_$(counter).txt")
    end

end

main(ARGS)
