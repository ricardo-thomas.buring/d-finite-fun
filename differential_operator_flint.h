#ifndef DIFFERENTIAL_OPERATOR_FLINT_H
#define DIFFERENTIAL_OPERATOR_FLINT_H

#include <flint/gr.h>
#include <flint/gr_poly.h>
#include <flint/gr_mat.h>

WARN_UNUSED_RESULT int gr_poly_mat_from_scalar_diffop(gr_mat_t A_numerator, gr_poly_t A_denominator, gr_poly_t A_scalarform, gr_ctx_t poly_ctx);
WARN_UNUSED_RESULT int gr_mat_poly_from_scalar_diffop(gr_poly_t A_numerator, gr_poly_t A_denominator, gr_poly_t A_scalarform, gr_ctx_t mat_ctx, gr_ctx_t poly_ctx);

WARN_UNUSED_RESULT int scalar_diffop_from_gr_poly_mat(gr_poly_t A_scalarform, gr_mat_t A_numerator, gr_poly_t A_denominator, gr_ctx_t poly_ctx);

#endif // DIFFERENTIAL_OPERATOR_FLINT_H
