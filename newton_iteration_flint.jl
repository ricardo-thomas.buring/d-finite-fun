using Nemo

include("FlintGenericRing.jl")

const lib_d_finite_fun = joinpath(@__DIR__, "libd-finite-fun_flint")

function poly_mat_solve_ode_newton_flint(A_numerator, A_denominator, Y0, order::Integer, method_flags::Integer)
    status = GR_SUCCESS

    R = base_ring(A_numerator)
    C = base_ring(R)

    # TODO: Make this more generic.
    A_coeff_ctx = gr_ctx_init_complex_acb(precision(C))
    A_poly_ctx = gr_ctx_init_gr_poly(A_coeff_ctx)

    sol_coeff_ctx = A_coeff_ctx
    sol_poly_ctx = A_poly_ctx

    A_numerator_flint = flint_gr_poly_mat_from_matrix(A_numerator, A_coeff_ctx, A_poly_ctx)
    A_denominator_flint = flint_gr_poly_from_polynomial(A_denominator, A_coeff_ctx)
    Y0_flint = flint_gr_mat_from_matrix(Y0, sol_coeff_ctx)

    Y_flint = gr_mat_init(size(A_numerator, 1), size(A_numerator, 1), sol_poly_ctx)
    status |= ccall((:gr_poly_mat_solve_ode_newton, lib_d_finite_fun), Cint,
                    (Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}, Clong, Cint, Ptr{Cvoid}, Ptr{Cvoid}),
                    Y_flint, A_numerator_flint, A_denominator_flint, Y0_flint, order, method_flags, A_poly_ctx, sol_poly_ctx)
    Y = matrix_from_flint_gr_poly_mat(Y_flint, sol_poly_ctx, R)

    gr_mat_clear(Y_flint, sol_poly_ctx)
    gr_mat_clear(Y0_flint, sol_coeff_ctx)
    gr_poly_clear(A_denominator_flint, A_coeff_ctx)
    gr_mat_clear(A_numerator_flint, A_poly_ctx)
    gr_ctx_clear(A_poly_ctx)
    gr_ctx_clear(A_coeff_ctx)

    return status, Y
end
