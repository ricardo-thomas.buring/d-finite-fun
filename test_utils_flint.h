#ifndef TEST_UTILS_FLINT_H
#define TEST_UTILS_FLINT_H

#include <flint/fmpz.h>
#include <flint/gr.h>
#include <flint/gr_mat.h>

#include "solve_ode_method.h"

WARN_UNUSED_RESULT int gr_poly_mat_and_denom_load_file(gr_mat_t A_numerator, gr_poly_t A_denominator, FILE *stream, gr_ctx_t poly_ctx);

typedef struct {
    double time_elapsed_solving;
    double time_elapsed_squaring_trace;
    fmpz radius_max_exp;
    int equation_satisfied;
    int initial_condition_satisfied;
} solve_ode_test_result_struct;
typedef solve_ode_test_result_struct solve_ode_test_result_t[1];

enum solve_ode_test_flags {
    SOLVE_ODE_TEST_WRITE_PROGRESS = 1 << 0,
    SOLVE_ODE_TEST_WRITE_SOLUTION = 1 << 1,
};

WARN_UNUSED_RESULT int test_gr_poly_mat_solve_scalar_ode(solve_ode_test_result_t res, int test_flags, gr_stream_t out, gr_poly_t A_scalarform, gr_ptr t0, gr_mat_t Y0, slong len, gr_poly_mat_solve_ode_method_t gr_poly_mat_solve_ode_method, char* method_name, int method_flags, gr_ctx_t A_poly_ctx, gr_ctx_t sol_poly_ctx);
WARN_UNUSED_RESULT int test_gr_mat_poly_solve_scalar_ode(solve_ode_test_result_t res, int test_flags, gr_stream_t out, gr_poly_t A_scalarform, gr_ptr t0, gr_mat_t Y0, slong len, gr_mat_poly_solve_ode_method_t gr_mat_poly_solve_ode_method, char* method_name, int method_flags, gr_ctx_t A_poly_ctx, gr_ctx_t sol_poly_ctx);

WARN_UNUSED_RESULT int test_gr_poly_mat_solve_ode(solve_ode_test_result_t res, int test_flags, gr_stream_t out, gr_mat_t A_numerator, gr_poly_t A_denominator, gr_ptr t0, gr_mat_t Y0, slong len, gr_poly_mat_solve_ode_method_t gr_poly_mat_solve_ode_method, char* method_name, int method_flags, gr_ctx_t A_poly_ctx, gr_ctx_t sol_poly_ctx);
WARN_UNUSED_RESULT int test_gr_mat_poly_solve_ode(solve_ode_test_result_t res, int test_flags, gr_stream_t out, gr_poly_t A_numerator, gr_poly_t A_denominator, gr_ptr t0, gr_mat_t Y0, slong len, gr_mat_poly_solve_ode_method_t gr_mat_poly_solve_ode_method, char* method_name, int method_flags, gr_ctx_t A_mat_ctx, gr_ctx_t sol_mat_ctx);

WARN_UNUSED_RESULT fmpz acb_poly_mat_radius_max_exp(gr_mat_t error_term, gr_ctx_t poly_ctx);
WARN_UNUSED_RESULT fmpz acb_mat_poly_radius_max_exp(gr_poly_t error_term, gr_ctx_t mat_ctx);

#endif // TEST_UTILS_FLINT_H
