using Nemo: acb

GR_SUCCESS = 0

T_TRUE = 0
T_FALSE = 1
T_UNKNOWN = 2

# Contexts.

GR_CTX_STRUCT_SIZE_IN_BYTES = 6*8 + 4*8

function gr_ctx_init_complex_acb(prec)
    ctx_data = Vector{UInt8}(undef, GR_CTX_STRUCT_SIZE_IN_BYTES)
    ccall((:gr_ctx_init_complex_acb, :libflint), Cvoid, (Ptr{Cvoid}, Clong), ctx_data, prec)
    return ctx_data
end

function gr_ctx_init_gr_poly(coeff_ctx)
    ctx_data = Vector{UInt8}(undef, GR_CTX_STRUCT_SIZE_IN_BYTES)
    ccall((:gr_ctx_init_gr_poly, :libflint), Cvoid, (Ptr{Cvoid}, Ptr{Cvoid}), ctx_data, coeff_ctx)
    return ctx_data
end

function gr_ctx_clear(ctx)
    ccall((:gr_ctx_clear, :libflint), Cvoid, (Ptr{Cvoid},), ctx)
end

# Polynomials.

GR_POLY_STRUCT_SIZE_IN_BYTES = 3*8

function gr_poly_init(coeff_ctx)
    poly_data = Vector{UInt8}(undef, GR_POLY_STRUCT_SIZE_IN_BYTES)
    ccall((:gr_poly_init, :libflint), Cvoid, (Ptr{Cvoid}, Ptr{Cvoid}), poly_data, coeff_ctx)
    return poly_data
end

function gr_poly_clear(poly_data, coeff_ctx)
    ccall((:gr_poly_clear, :libflint), Cvoid, (Ptr{Cvoid}, Ptr{Cvoid}), poly_data, coeff_ctx)
end

function gr_poly_zero(poly_data, coeff_ctx)
    return ccall((:gr_poly_zero, :libflint), Cint, (Ptr{Cvoid}, Ptr{Cvoid}), poly_data, coeff_ctx)
end

function gr_poly_one(poly_data, coeff_ctx)
    return ccall((:gr_poly_one, :libflint), Cint, (Ptr{Cvoid}, Ptr{Cvoid}), poly_data, coeff_ctx)
end

function gr_poly_set_coeff_si(poly_data, exponent, coeff, coeff_ctx)
    return ccall((:gr_poly_set_coeff_si, :libflint), Cint, (Ptr{Cvoid}, Clong, Clong, Ptr{Cvoid}), poly_data, exponent, coeff, coeff_ctx)
end

function gr_poly_set_coeff_scalar(poly_data, exponent, coeff, coeff_ctx)
    # TODO: Make this more generic (not just for acb).
    return ccall((:gr_poly_set_coeff_scalar, :libflint), Cint, (Ptr{Cvoid}, Clong, Ref{acb}, Ptr{Cvoid}), poly_data, exponent, coeff, coeff_ctx)
end

function gr_poly_set(poly_data, other_poly_data, coeff_ctx)
    return ccall((:gr_poly_set, :libflint), Cint, (Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}), poly_data, other_poly_data, coeff_ctx)
end

function gr_poly_print(poly_data, coeff_ctx)
    ccall((:gr_poly_print, :libflint), Cvoid, (Ptr{Cvoid}, Ptr{Cvoid}), poly_data, coeff_ctx)
end

function gr_poly_mul(result_data, poly_data, other_poly_data, coeff_ctx)
    return ccall((:gr_poly_mul, :libflint), Cint, (Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}), result_data, poly_data, other_poly_data, coeff_ctx)
end

# Matrices.

mutable struct gr_mat
    entries::Ptr{Cvoid}
    r::Int64
    c::Int64
    rows::Ptr{Cvoid}

    gr_mat() = new(C_NULL, 0, 0, C_NULL)
end

function gr_mat_init(rows, columns, coeff_ctx)
    mat_ref = Ref(gr_mat())
    ccall((:gr_mat_init, :libflint), Cvoid, (Ptr{gr_mat}, Clong, Clong, Ptr{Cvoid}), mat_ref, rows, columns, coeff_ctx)
    return mat_ref
end

function gr_mat_clear(mat_data, coeff_ctx)
    ccall((:gr_mat_clear, :libflint), Cvoid, (Ptr{Cvoid}, Ptr{Cvoid}), mat_data, coeff_ctx)
end

function gr_mat_zero(mat_data, coeff_ctx)
    return ccall((:gr_mat_zero, :libflint), Cint, (Ptr{Cvoid}, Ptr{Cvoid}), mat_data, coeff_ctx)
end

function gr_mat_one(mat_data, coeff_ctx)
    return ccall((:gr_mat_one, :libflint), Cint, (Ptr{Cvoid}, Ptr{Cvoid}), mat_data, coeff_ctx)
end

function gr_mat_entry_ptr(mat_data, i, j, coeff_ctx)
    return ccall((:gr_mat_entry_ptr, :libflint), Ptr{Cvoid}, (Ptr{Cvoid}, Clong, Clong, Ptr{Cvoid}), mat_data, i, j, coeff_ctx)
end

function gr_mat_print(mat_data, coeff_ctx)
    return ccall((:gr_mat_print, :libflint), Cint, (Ptr{Cvoid}, Ptr{Cvoid}), mat_data, coeff_ctx)
end

function gr_mat_trace(result_data, mat_data, coeff_ctx)
    return ccall((:gr_mat_trace, :libflint), Cint, (Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}), result_data, mat_data, coeff_ctx)
end

# Conversion functions.

function flint_gr_poly_from_polynomial(p, coeff_ctx)
    poly_data = gr_poly_init(coeff_ctx)
    for k in 0:degree(p)
        gr_poly_set_coeff_scalar(poly_data, k, coeff(p, k), coeff_ctx)
    end
    return poly_data
end

function flint_gr_mat_from_matrix(mat, coeff_ctx)
    mat_data = gr_mat_init(nrows(mat), ncols(mat), coeff_ctx)
    status = GR_SUCCESS
    status |= gr_mat_zero(mat_data, coeff_ctx)
    for I in eachindex(mat)
        i, j = I[1], I[2]
        entry_ptr = gr_mat_entry_ptr(mat_data, i - 1, j - 1, coeff_ctx)
        # TODO: Make this more generic (not just for acb).
        ccall((:acb_set, :libflint), Cint, (Ptr{Cvoid}, Ref{acb}), entry_ptr, mat[I])
    end
    return mat_data
end

function flint_gr_poly_mat_from_matrix(mat, coeff_ctx, poly_ctx)
    mat_data = gr_mat_init(nrows(mat), ncols(mat), poly_ctx)
    status = GR_SUCCESS
    status |= gr_mat_zero(mat_data, poly_ctx)
    for I in eachindex(mat)
        i, j = I[1], I[2]
        p = flint_gr_poly_from_polynomial(mat[I], coeff_ctx)
        gr_poly_set(gr_mat_entry_ptr(mat_data, i - 1, j - 1, poly_ctx), p, coeff_ctx)
        gr_poly_clear(p, coeff_ctx)
    end
    return mat_data
end

function matrix_from_flint_gr_poly_mat(mat, poly_ctx, poly_ring)
    # TODO: Make this more generic.
    A = zero_matrix(poly_ring, mat[].r, mat[].c)
    for i in 1:mat[].r
        for j in 1:mat[].c
            p = unsafe_load(Ptr{acb_poly}(gr_mat_entry_ptr(mat, i-1, j-1, poly_ctx)))
            p.parent = R
            A[i,j] = p
        end
    end
    return A
end
