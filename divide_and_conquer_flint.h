#ifndef DIVIDE_AND_CONQUER_FLINT_H
#define DIVIDE_AND_CONQUER_FLINT_H

#include <flint/gr.h>
#include <flint/gr_poly.h>
#include <flint/gr_mat.h>

WARN_UNUSED_RESULT int gr_poly_mat_solve_ode_divide_and_conquer(gr_mat_t Y, gr_mat_t A_numerator, gr_poly_t A_denominator, gr_mat_t Y0, slong len, int method_flags, gr_ctx_t A_poly_ctx, gr_ctx_t sol_poly_ctx);

WARN_UNUSED_RESULT int gr_mat_poly_solve_ode_divide_and_conquer(gr_poly_t Y, gr_poly_t A_numerator, gr_poly_t A_denominator, gr_mat_t Y0, slong len, int method_flags, gr_ctx_t A_mat_ctx, gr_ctx_t sol_mat_ctx);

#endif // DIVIDE_AND_CONQUER_FLINT_H
