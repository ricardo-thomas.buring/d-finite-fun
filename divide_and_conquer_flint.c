#include "divide_and_conquer_flint.h"
#include "solve_ode_method.h"
#include "gr_mat_poly.h"

#include <flint/gr_vec.h>

/*
 * This is a C/FLINT implementation of the divide and conquer algorithm described in:
 * Fast computation of power series solutions of systems of differential equations
 * by A. Bostan, F. Chyzak, F. Ollivier, B. Salvy, É. Schost and A. Sedoglavic
 * in Proceedings of the Eighteenth Annual ACM-SIAM Symposium on Discrete Algorithms, 2007, pp. 1012 -- 1021.
 */

// Compared with the algorithm cited above, the single solution vector is replaced by a matrix of solutions.
// Also, in the first recursive call to obtain the lower part of the solution we truncate the matrices A and S.
// We use an iteration for the inverse of the denominator of the matrix A, shared among steps with the same depth in the binary tree.
// The most costly polynomial matrix multiplication is optimized, making use of the structure of the left matrix.

/* Matrices with polynomial entries. Iteration for denominator inverse. */

// This is a specialized implementation of univariate polynomial matrix multiplication
//     (p*I + A)*B
// for the case where A = -z*(companion matrix) and the result is computed modulo z^m.
int gr_poly_mat_mul_specialized(gr_mat_t res, gr_mat_t A, gr_mat_t B, slong p, slong m, gr_ctx_t poly_ctx)
{
    int status = GR_SUCCESS;
    gr_ctx_struct *coeff_ctx = POLYNOMIAL_CTX(poly_ctx)->base_ring;
    gr_mat_t tmp_mat;
    gr_mat_init(tmp_mat, A->r, A->c, poly_ctx);
    status |= gr_mat_zero(tmp_mat, poly_ctx);
    gr_ptr tmp_poly;
    GR_TMP_INIT(tmp_poly, poly_ctx);
    status |= gr_poly_zero(tmp_poly, coeff_ctx);
    for (int i = 0; i < A->r - 1; i++) {
        for (int j = 0; j < A->c; j++) {
            gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, i, j, poly_ctx);
            gr_ptr entry_B1 = gr_mat_entry_ptr(B, i, j, poly_ctx);
            status |= gr_poly_mul_si(entry_tmp, entry_B1, p, coeff_ctx);
            // NOTE: Off-diagonal entry of A is -z, so shift left and subtract.
            gr_ptr entry_B2 = gr_mat_entry_ptr(B, i + 1, j, poly_ctx);
            status |= gr_poly_set(tmp_poly, entry_B2, coeff_ctx);
            status |= gr_poly_shift_left(tmp_poly, tmp_poly, 1, coeff_ctx);
            status |= gr_poly_sub(entry_tmp, entry_tmp, tmp_poly, coeff_ctx);
        }
    }
    for (int j = 0; j < A->c; j++) {
        gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, A->r - 1, j, poly_ctx);
        for (int i = 0; i < B->r; i++) {
            gr_ptr entry_A = gr_mat_entry_ptr(A, A->r - 1, i, poly_ctx);
            gr_ptr entry_B = gr_mat_entry_ptr(B, i, j, poly_ctx);
            // NOTE: Computing the product modulo z^m.
            status |= gr_poly_mullow(tmp_poly, entry_A, entry_B, m, coeff_ctx);
            status |= gr_poly_add(entry_tmp, entry_tmp, tmp_poly, coeff_ctx);
        }
    }
    GR_TMP_CLEAR(tmp_poly, poly_ctx);
    gr_mat_swap(res, tmp_mat, poly_ctx);
    gr_mat_clear(tmp_mat, poly_ctx);
    return status;
}

int gr_poly_mat_divide_and_conquer(gr_mat_t Y, gr_mat_t A_numerator1, gr_poly_t A_denominator, gr_poly_t A_denominator_inv, slong *A_denominator_inv_len, gr_mat_t S, slong p, slong m, gr_mat_t V, int method_flags, gr_ctx_t A_poly_ctx, gr_ctx_t sol_poly_ctx)
{
    int status = GR_SUCCESS;

    gr_ctx_struct *A_coeff_ctx = POLYNOMIAL_CTX(A_poly_ctx)->base_ring;
    gr_ctx_struct *sol_coeff_ctx = POLYNOMIAL_CTX(sol_poly_ctx)->base_ring;

    if (m == 1) {
        if (p == 0) {
            status |= gr_mat_set_gr_mat_other(Y, V, sol_coeff_ctx, sol_poly_ctx);
        } else {
            // Compute S(0)
            for (int i = 0; i < S->r; i++) {
                for (int j = 0; j < S->c; j++) {
                    gr_ptr entry_Y = gr_mat_entry_ptr(Y, i, j, sol_poly_ctx);
                    gr_ptr entry_S = gr_mat_entry_ptr(S, i, j, sol_poly_ctx);
                    status |= gr_poly_truncate(entry_Y, entry_S, 1, sol_coeff_ctx);
                }
            }
            // Y = (1/p) * S(0)
            status |= gr_mat_div_si(Y, Y, p, sol_poly_ctx);
        }
        return status;
    } else {
        slong d = m / 2;

        // S0 = S % z^d
        gr_mat_t S0;
        status |= gr_mat_init_set(S0, S, sol_poly_ctx);
        for (int i = 0; i < S0->r; i++) {
            for (int j = 0; j < S0->c; j++) {
                gr_ptr entry_S0 = gr_mat_entry_ptr(S0, i, j, sol_poly_ctx);
                status |= gr_poly_truncate(entry_S0, entry_S0, d, sol_coeff_ctx);
            }
        }

        // A_numerator0 = A_numerator1 % z^d
        gr_mat_t A_numerator0;
        status |= gr_mat_init_set(A_numerator0, A_numerator1, A_poly_ctx);
        for (int i = 0; i < A_numerator0->r; i++) {
            for (int j = 0; j < A_numerator0->c; j++) {
                gr_ptr entry_A = gr_mat_entry_ptr(A_numerator0, i, j, A_poly_ctx);
                status |= gr_poly_truncate(entry_A, entry_A, d, A_coeff_ctx);
            }
        }

        // Y0 = divide_and_conquer(A_numerator0, A_denominator, A_denominator_inv, A_denominator_inv_len, S0, p, d, V)
        // NOTE: We store Y0 in Y.
        status |= gr_poly_mat_divide_and_conquer(Y, A_numerator0, A_denominator, A_denominator_inv, A_denominator_inv_len, S0, p, d, V, method_flags, A_poly_ctx, sol_poly_ctx);

        // NOTE: The length of the inverse of A_denominator may have already been doubled in the first branch at the same depth.
        if ((method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) == 0 && *A_denominator_inv_len < m) {
            FLINT_ASSERT(*A_denominator_inv_len * 2 == m);
            // Iteration for A_denominator_inv mod z^m.
            gr_ptr tmp_poly;
            GR_TMP_INIT(tmp_poly, sol_poly_ctx);
            status |= gr_poly_truncate(tmp_poly, A_denominator, m, sol_coeff_ctx);
            // TODO: Use middle product here?
            status |= gr_poly_mullow(tmp_poly, tmp_poly, A_denominator_inv, m, sol_coeff_ctx);
            status |= gr_poly_shift_right(tmp_poly, tmp_poly, d, sol_coeff_ctx);
            status |= gr_poly_mullow(tmp_poly, tmp_poly, A_denominator_inv, d, sol_coeff_ctx);
            status |= gr_poly_shift_left(tmp_poly, tmp_poly, d, sol_coeff_ctx);
            status |= gr_poly_sub(A_denominator_inv, A_denominator_inv, tmp_poly, sol_coeff_ctx);
            *A_denominator_inv_len *= 2;
            GR_TMP_CLEAR(tmp_poly, sol_poly_ctx);
        }

        // Next we set S1 = S - z*Y0' - p*Y0 + z*A_denominator_inv*A_numerator1*Y0 in a few steps.

        // TODO: Support and optimize the case where Y0 is not a square matrix, having fewer columns.

        // S1 = -z*Y0'
        gr_mat_t S1;
        gr_mat_init(S1, Y->r, Y->c, sol_poly_ctx);
        for (int i = 0; i < S1->r; i++) {
            for (int j = 0; j < S1->c; j++) {
                gr_ptr entry_S1 = gr_mat_entry_ptr(S1, i, j, sol_poly_ctx);
                gr_ptr entry_Y0 = gr_mat_entry_ptr(Y, i, j, sol_poly_ctx);
                status |= gr_poly_derivative(entry_S1, entry_Y0, sol_coeff_ctx);
                status |= gr_poly_shift_left(entry_S1, entry_S1, 1, sol_coeff_ctx);
                status |= gr_poly_neg(entry_S1, entry_S1, sol_coeff_ctx);
            }
        }

        gr_mat_t tmp_mat;
        if (A_poly_ctx == sol_poly_ctx) {
            // We set S1 = S1 - (p*I - z*A_denominator_inv*A_numerator1)*Y0 in a few steps.

            // tmp_mat = -z*A_denominator_inv*A_numerator1
            status |= gr_mat_init_set(tmp_mat, A_numerator1, sol_poly_ctx);
            for (int i = 0; i < tmp_mat->r; i++) {
                for (int j = 0; j < tmp_mat->c; j++) {
                    gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, i, j, sol_poly_ctx);
                    // NOTE: Multiplication mod z^(m - 1).
                    if ((method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) == 0) {
                        status |= gr_poly_mullow(entry_tmp, entry_tmp, A_denominator_inv, m - 1, sol_coeff_ctx);
                    }
                    status |= gr_poly_shift_left(entry_tmp, entry_tmp, 1, sol_coeff_ctx);
                    status |= gr_poly_neg(entry_tmp, entry_tmp, sol_coeff_ctx);
                }
            }

            // tmp_mat = (p*I + tmp_mat)*Y0
            if (method_flags & SOLVE_ODE_METHOD_ASSUME_A_IS_COMPANION) {
                status |= gr_poly_mat_mul_specialized(tmp_mat, tmp_mat, Y, p, m, sol_poly_ctx);
            } else {
                status |= gr_mat_add_si(tmp_mat, tmp_mat, p, sol_poly_ctx);
                status |= gr_mat_mul(tmp_mat, tmp_mat, Y, sol_poly_ctx);
            }

            // S1 = S1 - tmp_mat
            status |= gr_mat_sub(S1, S1, tmp_mat, sol_poly_ctx);
        } else {
            // We set S1 = S1 - p*Y0 + z*A_denominator_inv*(A_numerator1*Y0) in a few steps.

            // S1 = S1 - p*Y0
            status |= gr_mat_init_set(tmp_mat, Y, sol_poly_ctx);
            status |= gr_mat_mul_si(tmp_mat, tmp_mat, p, sol_poly_ctx);
            status |= gr_mat_sub(S1, S1, tmp_mat, sol_poly_ctx);

            // tmp_mat = A_numerator1*Y0
            // TODO: Optimize this multiplication, e.g. when A is companion.
            status |= gr_mat_set_gr_mat_other(tmp_mat, A_numerator1, A_poly_ctx, sol_poly_ctx);
            // TODO: Mullow instead.
            status |= gr_mat_mul(tmp_mat, tmp_mat, Y, sol_poly_ctx);

            // tmp_mat = z*A_denominator_inv*tmp_mat
            for (int i = 0; i < tmp_mat->r; i++) {
                for (int j = 0; j < tmp_mat->c; j++) {
                    gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, i, j, sol_poly_ctx);
                    // NOTE: Multiplication mod z^(m - 1).
                    if ((method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) == 0) {
                        status |= gr_poly_mullow(entry_tmp, entry_tmp, A_denominator_inv, m - 1, sol_coeff_ctx);
                    }
                    status |= gr_poly_shift_left(entry_tmp, entry_tmp, 1, sol_coeff_ctx);
                }
            }

            // S1 = S1 + tmp_mat
            status |= gr_mat_add(S1, S1, tmp_mat, sol_poly_ctx);
        }

        // S1 = S1 + S
        status |= gr_mat_add(S1, S1, S, sol_poly_ctx);

        // S1 = S1 % z^m; S1 = div(S1, z^d)
        for (int i = 0; i < S1->r; i++) {
            for (int j = 0; j < S1->c; j++) {
                gr_ptr entry_S1 = gr_mat_entry_ptr(S1, i, j, sol_poly_ctx);
                // The specialized multiplication in the companion case already truncated.
                if (!(method_flags & SOLVE_ODE_METHOD_ASSUME_A_IS_COMPANION)) {
                    status |= gr_poly_truncate(entry_S1, entry_S1, m, sol_coeff_ctx);
                }
                status |= gr_poly_shift_right(entry_S1, entry_S1, d, sol_coeff_ctx);
            }
        }

        // Y1 = divide_and_conquer(A_numerator1, A_denominator, A_denominator_inv, A_denominator_inv_len, S1, p + d, m - d, V)
        // NOTE: We store Y1 in tmp_mat.
        status |= gr_poly_mat_divide_and_conquer(tmp_mat, A_numerator1, A_denominator, A_denominator_inv, A_denominator_inv_len, S1, p + d, m - d, V, method_flags, A_poly_ctx, sol_poly_ctx);

        // Y = Y0 + z^d*Y1
        for (int i = 0; i < tmp_mat->r; i++) {
            for (int j = 0; j < tmp_mat->c; j++) {
                gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, i, j, sol_poly_ctx);
                status |= gr_poly_shift_left(entry_tmp, entry_tmp, d, sol_coeff_ctx);
            }
        }
        status |= gr_mat_add(Y, Y, tmp_mat, sol_poly_ctx);

        gr_mat_clear(tmp_mat, sol_poly_ctx);
        gr_mat_clear(S1, sol_poly_ctx);
        gr_mat_clear(A_numerator0, A_poly_ctx);
        gr_mat_clear(S0, sol_poly_ctx);

        return status;
    }
}

int gr_poly_mat_solve_ode_divide_and_conquer(gr_mat_t Y, gr_mat_t A_numerator, gr_poly_t A_denominator, gr_mat_t Y0, slong len, int method_flags, gr_ctx_t A_poly_ctx, gr_ctx_t sol_poly_ctx)
{
    int status = GR_SUCCESS;

    gr_ctx_struct *A_coeff_ctx = POLYNOMIAL_CTX(A_poly_ctx)->base_ring;
    gr_ctx_struct *sol_coeff_ctx = POLYNOMIAL_CTX(sol_poly_ctx)->base_ring;

    gr_mat_t S;
    gr_mat_init(S, A_numerator->r, A_numerator->c, sol_poly_ctx);
    status |= gr_mat_zero(S, sol_poly_ctx);

    slong p = 0;

    gr_mat_t A_numerator1;
    status |= gr_mat_init_set(A_numerator1, A_numerator, A_poly_ctx);

    // Convert A_denominator into the solution context.
    gr_ptr A_denominator_sol;
    GR_TMP_INIT(A_denominator_sol, sol_poly_ctx);
    status |= gr_poly_set_gr_poly_other(A_denominator_sol, A_denominator, A_coeff_ctx, sol_coeff_ctx);

    // Initialize A_denominator_inv
    gr_ptr A_denominator_inv;
    GR_TMP_INIT(A_denominator_inv, sol_poly_ctx);
    slong A_denominator_inv_len;
    if (method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) {
        // Invert the polynomial A_denominator as a power series.
        status |= gr_poly_inv_series(A_denominator_inv, A_denominator_sol, len, sol_coeff_ctx);

        // TODO: Don't modify the input?
        // FIXME: This code assumes A_coeff_ctx == sol_coeff_ctx

        // Replace A_numerator by the power series expansion of A_numerator / A_denominator.
        for (int i = 0; i < A_numerator->r; i++) {
            for (int j = 0; j < A_numerator->c; j++) {
                gr_ptr entry_A = gr_mat_entry_ptr(A_numerator, i, j, sol_poly_ctx);
                status |= gr_poly_mullow(entry_A, entry_A, A_denominator_inv, len, sol_coeff_ctx);
            }
        }
        // Replace A_numerator1 by the power series expansion of A_numerator1 / A_denominator.
        for (int i = 0; i < A_numerator1->r; i++) {
            for (int j = 0; j < A_numerator1->c; j++) {
                gr_ptr entry_A1 = gr_mat_entry_ptr(A_numerator1, i, j, sol_poly_ctx);
                status |= gr_poly_mullow(entry_A1, entry_A1, A_denominator_inv, len, sol_coeff_ctx);
            }
        }
        // Replace A_denominator by one.
        status |= gr_poly_one(A_denominator, A_coeff_ctx);
        status |= gr_poly_one(A_denominator_sol, sol_coeff_ctx);
        status |= gr_poly_one(A_denominator_inv, sol_coeff_ctx);
        A_denominator_inv_len = 1;
    } else {
        // Calculate the first term of the power series inverse: 1 / A_denominator(0).
        status |= gr_poly_one(A_denominator_inv, sol_coeff_ctx);
        if (gr_poly_length(A_denominator, sol_coeff_ctx) > 0) {
            gr_ptr A_denominator0 = gr_poly_entry_ptr(A_denominator, 0, sol_coeff_ctx);
            status |= gr_poly_div_scalar(A_denominator_inv, A_denominator_inv, A_denominator0, sol_coeff_ctx);
            A_denominator_inv_len = 1;
        } else {
            status = GR_DOMAIN;
        }
    }

    if (status == GR_SUCCESS) {
        status |= gr_poly_mat_divide_and_conquer(Y, A_numerator1, A_denominator_sol, A_denominator_inv, &A_denominator_inv_len, S, p, len, Y0, method_flags, A_poly_ctx, sol_poly_ctx);
    }

    GR_TMP_CLEAR(A_denominator_inv, sol_poly_ctx);
    GR_TMP_CLEAR(A_denominator_sol, sol_poly_ctx);
    gr_mat_clear(A_numerator1, A_poly_ctx);
    gr_mat_clear(S, sol_poly_ctx);

    return status;
}

/* Polynomials with matrix coefficients. Iteration for denominator inverse. */

// This is a specialized implementation of the matrix polynomial multiplication
//     (p*I + A)*B
// for the case where A = -z*(companion matrix) and the result is computed modulo z^m.
int gr_mat_poly_mul_specialized(gr_poly_t res, gr_poly_t A, gr_poly_t B, slong p, slong m, gr_ctx_t mat_ctx)
{
    gr_ctx_struct *coeff_ctx = MATRIX_CTX(mat_ctx)->base_ring;

    int status = GR_SUCCESS;

    // [ 0 -z 0 0 ]   [ b11 b12 b13 b14 ] = [ -z*b21 -z*b22 -z*b23 -z*b24 ]
    // [ 0 0 -z 0 ] * [ b21 b22 b23 b24 ]   [ -z*b31 -z*b32 -z*b33 -z*b34 ]
    // [ 0 0 0 -z ]   [ b31 b31 b33 b34 ]   [ -z*b41 -z*b42 -z*b43 -z*b44 ]
    // [ p q r s  ]   [ b41 b42 b43 b44 ]   [ ...    ...    ...    ...    ]

    // TODO: Make this actually faster than gr_poly_mullow.

    gr_poly_t tmp_poly;
    gr_poly_init(tmp_poly, mat_ctx);

    // TODO: Do the shift by modifying the coefficients instead?

    // We first set tmp_poly = A*B mod z^m in a few steps.

    // tmp_poly = z*B mod z^m
    status |= gr_poly_truncate(tmp_poly, B, m - 1, mat_ctx);
    status |= gr_poly_shift_left(tmp_poly, tmp_poly, 1, mat_ctx);

    slong len_tmp = gr_poly_length(tmp_poly, mat_ctx);

    // deg(A*B) = deg(A) + deg(B) => length(A*B) = length(A) + length(B) - 1
    // TODO: Handle length(A) = length(B) = 0?
    slong newlen = FLINT_MIN(A->length + B->length - 1, m);
    gr_poly_fit_length(tmp_poly, newlen, mat_ctx);
    _gr_poly_set_length(tmp_poly, newlen, mat_ctx);
    // TODO: Avoid zeroing?
    slong sz = mat_ctx->sizeof_elem;
    status |= _gr_vec_zero(GR_ENTRY(tmp_poly->coeffs, len_tmp, sz), newlen - len_tmp, mat_ctx);

    // Next we are going to compute tmp_poly = p*B + A*B in a few steps.

    slong len_A = gr_poly_length(A, mat_ctx);
    slong len_B = gr_poly_length(B, mat_ctx);

    for (slong k = 0; k < newlen; k++) {
        gr_mat_struct *tmp_mat = (gr_mat_struct*)gr_poly_entry_ptr(tmp_poly, k, mat_ctx);
        gr_mat_struct *B_mat = NULL;
        if (k < len_B) {
            B_mat = (gr_mat_struct*)gr_poly_entry_ptr(B, k, mat_ctx);
        }
        // Set the top block of tmp_poly directly (all but the bottom row).
        for (slong i = 0; i < tmp_mat->r - 1; i++) {
            for (slong j = 0; j < tmp_mat->c; j++) {
                gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, i, j, coeff_ctx);
                if (B_mat != NULL) {
                    gr_ptr entry_B = gr_mat_entry_ptr(B_mat, i, j, coeff_ctx);
                    status |= gr_mul_si(entry_tmp, entry_B, p, coeff_ctx);
                } else {
                    status |= gr_zero(entry_tmp, coeff_ctx);
                }
                gr_ptr entry_tmp_below = gr_mat_entry_ptr(tmp_mat, i + 1, j, coeff_ctx);
                status |= gr_sub(entry_tmp, entry_tmp, entry_tmp_below, coeff_ctx);
            }
        }
        // Put p times the corresponding entry of B in the bottom row.
        if (B_mat != NULL) {
            for (slong j = 0; j < tmp_mat->c; j++) {
                gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, tmp_mat->r - 1, j, coeff_ctx);
                gr_ptr entry_B = gr_mat_entry_ptr(B_mat, B_mat->r - 1, j, coeff_ctx);
                status |= gr_mul_si(entry_tmp, entry_B, p, coeff_ctx);
            }
        } else {
            for (slong j = 0; j < tmp_mat->c; j++) {
                gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, tmp_mat->r - 1, j, coeff_ctx);
                status |= gr_zero(entry_tmp, coeff_ctx);
            }
        }
    }

    // Add the dot products in the bottom row.
    for (slong k = 0; k < newlen; k++) {
        gr_mat_struct *tmp_mat = (gr_mat_struct*)gr_poly_entry_ptr(tmp_poly, k, mat_ctx);
        for (slong k1 = 0; k1 < FLINT_MIN(len_A, k + 1); k1++) {
            gr_mat_struct *A_mat = (gr_mat_struct*)gr_poly_entry_ptr(A, k1, mat_ctx);
            slong k2 = k - k1;
            if (k2 >= len_B) {
                continue;
            }
            gr_mat_struct *B_mat = (gr_mat_struct*)gr_poly_entry_ptr(B, k2, mat_ctx);
            for (slong j = 0; j < A_mat->c; j++) {
                // In the jth column of the bottom row of the tmp_mat...
                gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, tmp_mat->r - 1, j, coeff_ctx);
                // ...add the dot product of the last row of A with the jth column of B.
                for (slong i = 0; i < B_mat->r; i++) {
                    gr_ptr entry_A = gr_mat_entry_ptr(A_mat, A_mat->r - 1, i, coeff_ctx);
                    gr_ptr entry_B = gr_mat_entry_ptr(B_mat, i, j, coeff_ctx);
                    status |= gr_addmul(entry_tmp, entry_A, entry_B, coeff_ctx);
                }
            }
        }
    }

    // TODO: Necessary?
    _gr_poly_normalise(tmp_poly, mat_ctx);

    // TODO: Do the whole operation in-place instead.
    // (using a temporary vector for a copy of the last row of A).
    status |= gr_poly_set(res, tmp_poly, mat_ctx);

    gr_poly_clear(tmp_poly, mat_ctx);

    return status;
}

int gr_mat_poly_divide_and_conquer(gr_poly_t Y, gr_poly_t A_numerator1, gr_poly_t A_denominator, gr_poly_t A_denominator_inv, slong *A_denominator_inv_len, gr_poly_t S, slong p, slong m, gr_mat_t V, int method_flags, gr_ctx_t A_mat_ctx, gr_ctx_t sol_mat_ctx)
{
    int status = GR_SUCCESS;

    gr_ctx_struct *sol_coeff_ctx = MATRIX_CTX(sol_mat_ctx)->base_ring;

    if (m == 1) {
        if (p == 0) {
            status |= gr_poly_set_scalar(Y, V, sol_mat_ctx);
        } else {
            // Compute S(0)
            status |= gr_poly_truncate(Y, S, 1, sol_mat_ctx);
            // Y = (1/p) * S(0)
            gr_ptr S0 = gr_poly_entry_ptr(Y, 0, sol_mat_ctx);
            status |= gr_mat_div_si(S0, S0, p, sol_coeff_ctx);
        }
#ifdef DEBUG_ENABLED
        printf("Debug: gr_mat_poly_divide_and_conquer");
        printf("\nm = %ld", m);
        printf("\np = %ld", p);
        printf("\nS = ");
        gr_poly_print(S, sol_mat_ctx);
        printf("\nY = ");
        gr_poly_print(Y, sol_mat_ctx);
        printf("\n\n");
#endif
        return status;
    } else {
        slong d = m / 2;

        // S0 = S % z^d
        gr_poly_t S0;
        gr_poly_init(S0, sol_mat_ctx);
        status |= gr_poly_truncate(S0, S, d, sol_mat_ctx);

        // A_numerator0 = A_numerator1 % z^d
        gr_poly_t A_numerator0;
        gr_poly_init(A_numerator0, A_mat_ctx);
        status |= gr_poly_truncate(A_numerator0, A_numerator1, d, A_mat_ctx);

        // Y0 = divide_and_conquer(A_numerator0, A_denominator, A_denominator_inv, A_denominator_inv_len, S0, p, d, V)
        // NOTE: We store Y0 in Y.
        status |= gr_mat_poly_divide_and_conquer(Y, A_numerator0, A_denominator, A_denominator_inv, A_denominator_inv_len, S0, p, d, V, method_flags, A_mat_ctx, sol_mat_ctx);

        // NOTE: The length of the inverse of A_denominator may have already been doubled in the first branch at the same depth.
        if ((method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) == 0 && *A_denominator_inv_len < m) {
            FLINT_ASSERT(*A_denominator_inv_len * 2 == m);
            // Iteration for A_denominator_inv mod z^m.
            gr_poly_t tmp_poly;
            gr_poly_init(tmp_poly, sol_coeff_ctx);
            status |= gr_poly_truncate(tmp_poly, A_denominator, m, sol_coeff_ctx);
            // TODO: Use middle product here?
            status |= gr_poly_mullow(tmp_poly, tmp_poly, A_denominator_inv, m, sol_coeff_ctx);
            status |= gr_poly_shift_right(tmp_poly, tmp_poly, d, sol_coeff_ctx);
            status |= gr_poly_mullow(tmp_poly, tmp_poly, A_denominator_inv, d, sol_coeff_ctx);
            status |= gr_poly_shift_left(tmp_poly, tmp_poly, d, sol_coeff_ctx);
            status |= gr_poly_sub(A_denominator_inv, A_denominator_inv, tmp_poly, sol_coeff_ctx);
            *A_denominator_inv_len *= 2;
            gr_poly_clear(tmp_poly, sol_coeff_ctx);
        }

        // TODO: Optimize the computation/slicing of S1 below.

#ifdef DEBUG_ENABLED
        printf("Debug: gr_mat_poly_divide_and_conquer");
        printf("\nm = %ld", m);
        printf("\np = %ld", p);
        printf("\nY0 = ");
        gr_poly_print(Y, sol_mat_ctx);
        printf("\nS = ");
        gr_poly_print(S, sol_mat_ctx);
        printf("\nA_denominator_inv = ");
        gr_poly_print(A_denominator_inv, sol_coeff_ctx);
#endif

        // Next we set S1 = S - z*Y0' - (p*I - z*A_denominator_inv*A_numerator1)*Y0 in a few steps.

        // TODO: Support and optimize the case where Y0 is not a square matrix, having fewer columns.

        // S1 = -z*Y0'
        gr_poly_t S1;
        gr_poly_init(S1, sol_mat_ctx);
        status |= gr_poly_set(S1, Y, sol_mat_ctx);
        status |= gr_poly_derivative(S1, S1, sol_mat_ctx);
        status |= gr_poly_shift_left(S1, S1, 1, sol_mat_ctx);
        status |= gr_poly_neg(S1, S1, sol_mat_ctx);

        gr_poly_t tmp_poly;
        gr_poly_init(tmp_poly, sol_mat_ctx);
        if (A_mat_ctx == sol_mat_ctx) {
            // Next we set S1 = S1 - (p*I - z*A_denominator_inv*A_numerator1)*Y0 in a few steps.

            // tmp_poly = A_denominator_inv*A_numerator1
            status |= gr_poly_set(tmp_poly, A_numerator1, sol_mat_ctx);
            if ((method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) == 0) {
                // NOTE: Multiplication mod z^(m - 1).
                status |= gr_mat_poly_mullow_poly(tmp_poly, tmp_poly, A_denominator_inv, m - 1, sol_mat_ctx);
            }

#ifdef DEBUG_ENABLED
            printf("\nA_truncated1 = ");
            gr_poly_print(tmp_poly, sol_mat_ctx);
#endif

            // tmp_poly = p*I - z*tmp_poly
            status |= gr_poly_shift_left(tmp_poly, tmp_poly, 1, sol_mat_ctx);
            status |= gr_poly_neg(tmp_poly, tmp_poly, sol_mat_ctx);
            status |= gr_poly_add_si(tmp_poly, tmp_poly, p, sol_mat_ctx);

            // tmp_poly = tmp_poly*Y0
            // TODO: Optimize this multiplication, e.g. when A is companion.
            if (method_flags & SOLVE_ODE_METHOD_ASSUME_A_IS_COMPANION) {
                status |= gr_mat_poly_mul_specialized(tmp_poly, tmp_poly, Y, p, m, sol_mat_ctx);
            } else {
                status |= gr_poly_mullow(tmp_poly, tmp_poly, Y, m, sol_mat_ctx);
            }

            // S1 = S1 - tmp_poly
            status |= gr_poly_sub(S1, S1, tmp_poly, sol_mat_ctx);
        } else {
            // Next we set S1 = S1 - p*Y0 + z*A_denominator_inv*(A_numerator1*Y0) in a few steps.

            // S1 = S1 - p*Y0
            status |= gr_poly_set(tmp_poly, Y, sol_mat_ctx);
            status |= gr_poly_mul_si(tmp_poly, tmp_poly, p, sol_mat_ctx);
            status |= gr_poly_sub(S1, S1, tmp_poly, sol_mat_ctx);

            // tmp_poly = A_numerator1*Y0
            // TODO: Optimize this multiplication, e.g. when A is companion.
            status |= gr_poly_set_gr_poly_other(tmp_poly, A_numerator1, A_mat_ctx, sol_mat_ctx);
            status |= gr_poly_mullow(tmp_poly, tmp_poly, Y, m, sol_mat_ctx);
            // tmp_poly = A_denominator_inv*tmp_poly mod z^(m-1)
            if ((method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) == 0) {
                status |= gr_mat_poly_mullow_poly(tmp_poly, tmp_poly, A_denominator_inv, m - 1, sol_mat_ctx);
            }

#ifdef DEBUG_ENABLED
            printf("\nA_truncated1 = ");
            gr_poly_print(tmp_poly, sol_mat_ctx);
#endif

            // tmp_poly = z*tmp_poly
            status |= gr_poly_shift_left(tmp_poly, tmp_poly, 1, sol_mat_ctx);

            // S1 = S1 + tmp_poly
            status |= gr_poly_add(S1, S1, tmp_poly, sol_mat_ctx);
        }

        status |= gr_poly_add(S1, S1, S, sol_mat_ctx);

        // S1 = S1 % z^m; S1 = div(S1, z^d)
        status |= gr_poly_truncate(S1, S1, m, sol_mat_ctx); // TODO: Necessary?
        status |= gr_poly_shift_right(S1, S1, d, sol_mat_ctx);

        // Y1 = divide_and_conquer(A_numerator1, A_denominator, A_denominator_inv, A_denominator_inv_len, S1, p + d, m - d, V)
        // NOTE: We store Y1 in tmp_poly.

#ifdef DEBUG_ENABLED
        printf("\nS1 = ");
        gr_poly_print(S1, sol_mat_ctx);
        printf("\n\n");
#endif

        status |= gr_mat_poly_divide_and_conquer(tmp_poly, A_numerator1, A_denominator, A_denominator_inv, A_denominator_inv_len, S1, p + d, m - d, V, method_flags, A_mat_ctx, sol_mat_ctx);

        // Y = Y0 + z^d*Y1
        status |= gr_poly_shift_left(tmp_poly, tmp_poly, d, sol_mat_ctx);
        status |= gr_poly_add(Y, Y, tmp_poly, sol_mat_ctx);

        gr_poly_clear(tmp_poly, sol_mat_ctx);
        gr_poly_clear(S1, sol_mat_ctx);
        gr_poly_clear(A_numerator0, A_mat_ctx);
        gr_poly_clear(S0, sol_mat_ctx);

        return status;
    }
}

int gr_mat_poly_solve_ode_divide_and_conquer(gr_poly_t Y, gr_poly_t A_numerator, gr_poly_t A_denominator, gr_mat_t Y0, slong len, int method_flags, gr_ctx_t A_mat_ctx, gr_ctx_t sol_mat_ctx)
{
    int status = GR_SUCCESS;

    gr_ctx_struct *A_coeff_ctx = MATRIX_CTX(A_mat_ctx)->base_ring;
    gr_ctx_struct *sol_coeff_ctx = MATRIX_CTX(sol_mat_ctx)->base_ring;

    gr_poly_t S;
    gr_poly_init(S, sol_mat_ctx);
    status |= gr_poly_zero(S, sol_mat_ctx);

    slong p = 0;

    gr_poly_t A_numerator1;
    gr_poly_init(A_numerator1, A_mat_ctx);
    status |= gr_poly_set(A_numerator1, A_numerator, A_mat_ctx);

#ifdef DEBUG_ENABLED
    printf("Debug: gr_mat_poly_solve_ode_divide_and_conquer");
    printf("\nA_numerator = ");
    gr_poly_print(A_numerator, sol_mat_ctx);

    printf("\nA_denominator = ");
    gr_poly_print(A_denominator, sol_coeff_ctx);
    printf("\n\n");
#endif

    // Convert A_denominator into the solution context.
    gr_poly_t A_denominator_sol;
    gr_poly_init(A_denominator_sol, sol_coeff_ctx);
    if (A_mat_ctx == sol_mat_ctx) {
        status |= gr_poly_set(A_denominator_sol, A_denominator, sol_coeff_ctx);
    } else {
        status |= gr_poly_set_gr_poly_other(A_denominator_sol, A_denominator, A_coeff_ctx, sol_coeff_ctx);
    }

    // Initialize A_denominator_inv
    gr_poly_t A_denominator_inv;
    gr_poly_init(A_denominator_inv, sol_coeff_ctx);
    slong A_denominator_inv_len;
    if (method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) {
        // Invert the polynomial A_denominator as a power series.
        status |= gr_poly_inv_series(A_denominator_inv, A_denominator_sol, len, sol_coeff_ctx);

        // TODO: Don't modify the input?
        // FIXME: This code assumes A_mat_ctx == sol_mat_ctx

        // Replace A_numerator by the power series expansion of A_numerator / A_denominator.
        status |= gr_mat_poly_mullow_poly(A_numerator, A_numerator, A_denominator_inv, len, sol_mat_ctx);

        // Replace A_numerator1 by the power series expansion of A_numerator1 / A_denominator.
        status |= gr_mat_poly_mullow_poly(A_numerator1, A_numerator1, A_denominator_inv, len, sol_mat_ctx);

        // Replace A_denominator by one.
        status |= gr_poly_one(A_denominator, A_coeff_ctx);
        status |= gr_poly_one(A_denominator_sol, sol_coeff_ctx);
        status |= gr_poly_one(A_denominator_inv, sol_coeff_ctx);
        A_denominator_inv_len = 1;
    } else {
        // Calculate the first term of the power series inverse: 1 / A_denominator(0).
        status |= gr_poly_one(A_denominator_inv, sol_coeff_ctx);
        if (gr_poly_length(A_denominator, sol_coeff_ctx) > 0) {
            gr_ptr A_denominator0 = gr_poly_entry_ptr(A_denominator, 0, sol_coeff_ctx);
            status |= gr_poly_div_scalar(A_denominator_inv, A_denominator_inv, A_denominator0, sol_coeff_ctx);
            A_denominator_inv_len = 1;
        } else {
            status = GR_DOMAIN;
        }
    }

    if (status == GR_SUCCESS) {
        status |= gr_mat_poly_divide_and_conquer(Y, A_numerator1, A_denominator_sol, A_denominator_inv, &A_denominator_inv_len, S, p, len, Y0, method_flags, A_mat_ctx, sol_mat_ctx);
    }

    gr_poly_clear(A_denominator_inv, sol_coeff_ctx);
    gr_poly_clear(A_denominator_sol, sol_coeff_ctx);
    gr_poly_clear(A_numerator1, A_mat_ctx);
    gr_poly_clear(S, sol_mat_ctx);

    return status;
}
