using Nemo

# Matrices with polynomial entries.


function poly_mat_from_scalar_diffop(A_scalarform)
    order = degree(A_scalarform)
    R = base_ring(A_scalarform)
    A_denominator = coeff(A_scalarform, order)
    A_numerator = zero_matrix(R, order, order)
    for i in 1:(order-1)
        A_numerator[i, i+1] = A_denominator
    end
    for i in 1:order
        A_numerator[order, i] = -coeff(A_scalarform, i-1)
    end
    return A_numerator, A_denominator
end


function poly_mat_taylor_shift(A, t0)
    t = gen(base_ring(A))
    return subst.(A, t + t0)
end


function poly_contains_zero(p::PolyRingElem{T}) where T <: RingElement
    return all(contains_zero(c) for c in coefficients(p))
end


function poly_mat_contains_zero(A)
    return all(poly_contains_zero.(A))
end


# Polynomials with matrix coefficients.


function mat_poly_from_scalar_diffop(A_scalarform)
    order = degree(A_scalarform)
    S = base_ring(A_scalarform)
    A_denominator = coeff(A_scalarform, order)
    MR = matrix_ring(base_ring(S), order)
    R, z = polynomial_ring(MR, var(S))
    A_numerator = zero(R)
    max_deg = maximum(degree(coeff(A_scalarform, i)) for i in 0:order)
    for d in 0:max_deg
        A_d = zero(MR)
        for i in 1:(order - 1)
            A_d[i, i+1] = coeff(A_denominator, d)
        end
        for i in 1:order
            A_d[order, i] = -coeff(coeff(A_scalarform, i-1), d)
        end
        setcoeff!(A_numerator, d, A_d)
    end
    return A_numerator, A_denominator
end


function mat_poly_taylor_shift(A, t0)
    t = gen(parent(A))
    return evaluate(A, t + t0)
end


function mat_poly_mul_poly(A, f)
    t = gen(parent(A))
    return sum(coeff(f, i)*t^i for i in 0:degree(f)) * A
end


function mat_poly_integral(A)
    t = gen(parent(A))
    return sum(coeff(A, i)*t^(i+1) / (i+1) for i in 0:degree(A))
end


function mat_contains_zero(A)
    return all(contains_zero(A[i]) for i in eachindex(A))
end


function mat_poly_contains_zero(A)
    return all(mat_contains_zero(coeff(A, i)) for i in 0:degree(A))
end


# TODO: poly_mat_to_mat_poly, mat_poly_to_poly_mat
