#include "test_utils_flint.h"
#include "differential_operator_flint.h"
#include "gr_mat_poly.h"

#include <string.h>
#include <time.h>
#include <flint/gr_mat.h>
#include <flint/gr_poly.h>
#include <flint/acb.h>

static const char *solve_ode_method_flag_descriptions[2] = {
    "assuming A is companion",
    "pre-computing A as a power series"
};

int gr_poly_mat_and_denom_load_file(gr_mat_t A_numerator, gr_poly_t A_denominator, FILE *stream, gr_ctx_t poly_ctx)
{
    int status = GR_SUCCESS;

    const int LINE_BUFFER_SIZE = 1 << 18;
    char line[LINE_BUFFER_SIZE];

    // Load header: rows cols type
    int rows = 0;
    if (fscanf(stream, "%d", &rows) != 1) {
        // fprintf(stderr, "Unable to read rows");
        return GR_UNABLE;
    }
    if (fgetc(stream) != ' ') {
        // fprintf(stderr, "Expected space after rows\n");
        return GR_UNABLE;
    }
    int cols = 0;
    if (fscanf(stream, "%d", &cols) != 1) {
        // fprintf(stderr, "Unable to read cols\n");
        return GR_UNABLE;
    }
    if (fgetc(stream) != ' ') {
        // fprintf(stderr, "Expected space after cols\n");
        return GR_UNABLE;
    }

    if (rows <= 0 || cols <= 0) {
        // fprintf(stderr, "Read invalid row or column count (rows = %d, cols = %d)\n", rows, cols);
        return GR_UNABLE;
    }

    gr_mat_init(A_numerator, rows, cols, poly_ctx);
    status |= gr_mat_zero(A_numerator, poly_ctx);

    // Read until end of first line.
    if (fgets(line, LINE_BUFFER_SIZE, stream) == NULL) {
        // fprintf(stderr, "Unable to read to end of first line\n");
        return GR_UNABLE;
    }

    int i = 0;
    int j = 0;
    while (1) {
        // Load the row and the column index.
        if (fscanf(stream, "%d %d ", &i, &j) != 2) {
            status = GR_UNABLE;
            break;
        }
        // Load the matrix entry.
        if (fgets(line, LINE_BUFFER_SIZE, stream) == NULL) {
            status = GR_UNABLE;
            break;
        }
        // Remove trailing newline.
        size_t line_length = strlen(line);
        line[line_length - 1] = '\0';
        if (i == 0 && j == 0 && strcmp(line, "0") == 0) {
            break;
        }
        status |= gr_set_str(gr_mat_entry_ptr(A_numerator, i, j, poly_ctx), line, poly_ctx);
    }

    if (status == GR_SUCCESS) {
        // Load the denominator.
        if (fgets(line, LINE_BUFFER_SIZE, stream) != NULL) {
            size_t line_length = strlen(line);
            line[line_length - 1] = '\0';
            status |= gr_set_str(A_denominator, line, poly_ctx);
        } else {
            status = GR_UNABLE;
        }
    }

    return status;
}

int test_gr_poly_mat_solve_scalar_ode(solve_ode_test_result_t res, int test_flags, gr_stream_t out, gr_poly_t A_scalarform, gr_ptr t0, gr_mat_t Y0, slong len, gr_poly_mat_solve_ode_method_t gr_poly_mat_solve_ode_method, char* method_name, int method_flags, gr_ctx_t A_poly_ctx, gr_ctx_t sol_poly_ctx)
{
    int status = GR_SUCCESS;

    gr_ctx_struct *A_coeff_ctx = POLYNOMIAL_CTX(A_poly_ctx)->base_ring;

    slong diff_order = gr_poly_length(A_scalarform, A_poly_ctx) - 1;

    gr_mat_t A_numerator;
    gr_mat_init(A_numerator, diff_order, diff_order, A_poly_ctx);

    gr_poly_t A_denominator;
    gr_poly_init(A_denominator, A_coeff_ctx);

    status |= gr_poly_mat_from_scalar_diffop(A_numerator, A_denominator, A_scalarform, A_poly_ctx);

    status |= test_gr_poly_mat_solve_ode(res, test_flags, out, A_numerator, A_denominator, t0, Y0, len, gr_poly_mat_solve_ode_method, method_name, method_flags, A_poly_ctx, sol_poly_ctx);

    gr_poly_clear(A_denominator, A_coeff_ctx);
    gr_mat_clear(A_numerator, A_poly_ctx);

    return status;
}

int test_gr_poly_mat_solve_ode(solve_ode_test_result_t res, int test_flags, gr_stream_t out, gr_mat_t A_numerator, gr_poly_t A_denominator, gr_ptr t0, gr_mat_t Y0, slong len, gr_poly_mat_solve_ode_method_t gr_poly_mat_solve_ode_method, char *method_name, int method_flags, gr_ctx_t A_poly_ctx, gr_ctx_t sol_poly_ctx)
{
    int status = GR_SUCCESS;

    gr_ctx_struct *A_coeff_ctx = POLYNOMIAL_CTX(A_poly_ctx)->base_ring;
    gr_ctx_struct *sol_coeff_ctx = POLYNOMIAL_CTX(sol_poly_ctx)->base_ring;

    // Shift A_numerator and A_denominator.
    gr_mat_t A_numerator_shifted;
    status |= gr_mat_init_set(A_numerator_shifted, A_numerator, A_poly_ctx);
    for (int i = 0; i < A_numerator_shifted->r; i++) {
        for (int j = 0; j < A_numerator_shifted->c; j++) {
            gr_ptr entry_A = gr_mat_entry_ptr(A_numerator_shifted, i, j, A_poly_ctx);
            status |= gr_poly_taylor_shift(entry_A, entry_A, t0, A_coeff_ctx);
        }
    }
    gr_poly_t A_denominator_shifted;
    gr_poly_init(A_denominator_shifted, A_coeff_ctx);
    status |= gr_poly_taylor_shift(A_denominator_shifted, A_denominator, t0, A_coeff_ctx);

    if (status != GR_SUCCESS) {
        if (test_flags & SOLVE_ODE_TEST_WRITE_PROGRESS) {
            gr_stream_write(out, "Failed to construct input for ");
            gr_stream_write(out, method_name);
            gr_stream_write(out, ".\n");
        }
        return status;
    }

    gr_mat_t Y;
    gr_mat_init(Y, A_numerator->r, A_numerator->c, sol_poly_ctx);

    struct timespec start;
    clock_gettime(CLOCK_MONOTONIC, &start);

    status |= gr_poly_mat_solve_ode_method(Y, A_numerator_shifted, A_denominator_shifted, Y0, len, method_flags, A_poly_ctx, sol_poly_ctx);

    struct timespec finish;
    clock_gettime(CLOCK_MONOTONIC, &finish);

    res->time_elapsed_solving = finish.tv_sec - start.tv_sec;
    res->time_elapsed_solving += (finish.tv_nsec - start.tv_nsec) / 1e9;

    if (status != GR_SUCCESS) {
        if (test_flags & SOLVE_ODE_TEST_WRITE_PROGRESS) {
            gr_stream_write(out, method_name);
            gr_stream_write(out, " failed.\n");
        }
        return status;
    }

    if (test_flags & SOLVE_ODE_TEST_WRITE_SOLUTION) {
        gr_stream_write(out, "Y = ");
        gr_mat_write(out, Y, sol_poly_ctx);
        gr_stream_write(out, "\n\n");
    }

    // See how long it takes to square tr(Y).

    gr_poly_t trace_Y_squared;
    gr_poly_init(trace_Y_squared, sol_coeff_ctx);
    status |= gr_mat_trace(trace_Y_squared, Y, sol_poly_ctx);

    if (test_flags & SOLVE_ODE_TEST_WRITE_SOLUTION) {
        gr_stream_write(out, "tr(Y) = ");
        gr_poly_write(out, trace_Y_squared, "t", sol_coeff_ctx);
        gr_stream_write(out, "\n\n");
    }

    clock_gettime(CLOCK_MONOTONIC, &start);

    status |= gr_poly_mul(trace_Y_squared, trace_Y_squared, trace_Y_squared, sol_coeff_ctx);

    clock_gettime(CLOCK_MONOTONIC, &finish);

    res->time_elapsed_squaring_trace = finish.tv_sec - start.tv_sec;
    res->time_elapsed_squaring_trace += (finish.tv_nsec - start.tv_nsec) / 1e9;

    int success = 1;

    // Test initial condition is satisfied.

    gr_mat_t Y_at_0;
    gr_mat_init(Y_at_0, Y->r, Y->c, sol_coeff_ctx);
    for (int i = 0; i < Y->r; i++) {
        for (int j = 0; j < Y->c; j++) {
            gr_ptr entry_Y = gr_mat_entry_ptr(Y, i, j, sol_poly_ctx);
            gr_ptr entry_Y_at_0 = gr_mat_entry_ptr(Y_at_0, i, j, sol_coeff_ctx);
            if (gr_poly_length(entry_Y, sol_coeff_ctx) > 0) {
                gr_ptr entry_Y_constant_coeff = gr_poly_entry_ptr(entry_Y, 0, sol_coeff_ctx);
                status |= gr_set(entry_Y_at_0, entry_Y_constant_coeff, sol_coeff_ctx);
            } else {
                status |= gr_zero(entry_Y_at_0, sol_coeff_ctx);
            }
        }
    }
    int ic_satisfied = gr_mat_equal(Y_at_0, Y0, sol_coeff_ctx) != T_FALSE;
    if (!ic_satisfied) {
        success = 0;
    }
    res->initial_condition_satisfied = ic_satisfied;
    gr_mat_clear(Y_at_0, sol_coeff_ctx);

    // Test by computing (A_denominator_shifted*Y' - A_numerator_shifted*Y) mod t^(len - 1).

    gr_mat_t error_term;
    gr_mat_init(error_term, A_numerator_shifted->r, A_numerator_shifted->c, sol_poly_ctx);

    gr_mat_t d;
    status |= gr_mat_init_set(d, Y, sol_poly_ctx);
    for (int i = 0; i < d->r; i++) {
        for (int j = 0; j < d->c; j++) {
            gr_ptr entry_d = gr_mat_entry_ptr(d, i, j, sol_poly_ctx);
            status |= gr_poly_derivative(entry_d, entry_d, sol_coeff_ctx);
        }
    }

    // TODO: Use mullow here.
    if (A_poly_ctx == sol_poly_ctx) {
        status |= gr_mat_mul_scalar(d, d, A_denominator_shifted, sol_poly_ctx);
        status |= gr_mat_set(error_term, A_numerator_shifted, sol_poly_ctx);
    } else {
        status |= gr_mat_mul_scalar_other(d, d, A_denominator_shifted, A_poly_ctx, sol_poly_ctx);
        status |= gr_mat_set_gr_mat_other(error_term, A_numerator_shifted, A_poly_ctx, sol_poly_ctx);
    }

    status |= gr_mat_mul(error_term, error_term, Y, sol_poly_ctx);
    status |= gr_mat_sub(error_term, d, error_term, sol_poly_ctx);
    for (int i = 0; i < error_term->r; i++) {
        for (int j = 0; j < error_term->c; j++) {
            gr_ptr entry_tmp = gr_mat_entry_ptr(error_term, i, j, sol_poly_ctx);
            status |= gr_poly_truncate(entry_tmp, entry_tmp, len - 1, sol_coeff_ctx);
        }
    }

    res->equation_satisfied = 1;
    if (gr_mat_is_zero(error_term, sol_poly_ctx) == T_FALSE) {
        res->equation_satisfied = 0;
        success = 0;
    }

    if (sol_coeff_ctx->which_ring == GR_CTX_CC_ACB) {
        res->radius_max_exp = acb_poly_mat_radius_max_exp(Y, sol_poly_ctx);
        if (res->radius_max_exp > 0) {
            success = 0;
        }
    }

    if (test_flags & SOLVE_ODE_TEST_WRITE_PROGRESS) {
        gr_stream_write(out, method_name);

        if (method_flags != 0) {
            gr_stream_write(out, " (");
            int method_flags_unseen = method_flags;
            int flag = 1;
            for (int i = 0; i < SOLVE_ODE_METHOD_FLAG_COUNT; i++) {
                if (method_flags_unseen & flag) {
                    gr_stream_write(out, solve_ode_method_flag_descriptions[i]);
                    method_flags_unseen &= ~flag;
                    if (method_flags_unseen != 0) {
                        gr_stream_write(out, ", ");
                    }
                }
                flag <<= 1;
            }
            gr_stream_write(out, ")");
        }

        if (success) {
            gr_stream_write(out, " succeeded.\n");
        } else {
            gr_stream_write(out, " failed.\n");
        }

        if (res->equation_satisfied == 0) {
            gr_stream_write(out, "- Error: The solution does not satisfy the original equation.\n");
        }

        if (res->initial_condition_satisfied == 0) {
            gr_stream_write(out, "- Error: The solution does not satisfy the initial condition.\n");
        }

        if (sol_coeff_ctx->which_ring == GR_CTX_CC_ACB) {
            gr_stream_write(out, "- The solution contains polynomials having coefficients with radius exponent <= ");
            gr_stream_write_fmpz(out, &res->radius_max_exp);
            gr_stream_write(out, ".\n");
        }

        gr_stream_write(out, "- Time to solve for Y: ");
        char time_elapsed_str[128];
        sprintf(time_elapsed_str, "%lf", res->time_elapsed_solving);
        gr_stream_write(out, time_elapsed_str);
        gr_stream_write(out, " seconds\n");

        gr_stream_write(out, "- Time to square tr(Y): ");
        sprintf(time_elapsed_str, "%lf", res->time_elapsed_squaring_trace);
        gr_stream_write(out, time_elapsed_str);
        gr_stream_write(out, " seconds\n");
    }

    gr_mat_clear(error_term, sol_poly_ctx);
    gr_mat_clear(d, sol_poly_ctx);
    gr_poly_clear(trace_Y_squared, sol_coeff_ctx);
    gr_mat_clear(Y, sol_poly_ctx);
    gr_poly_clear(A_denominator_shifted, A_coeff_ctx);
    gr_mat_clear(A_numerator_shifted, A_poly_ctx);

    return status;
}

int test_gr_mat_poly_solve_scalar_ode(solve_ode_test_result_t res, int test_flags, gr_stream_t out, gr_poly_t A_scalarform, gr_ptr t0, gr_mat_t Y0, slong len, gr_mat_poly_solve_ode_method_t gr_mat_poly_solve_ode_method, char* method_name, int method_flags, gr_ctx_t A_poly_ctx, gr_ctx_t sol_poly_ctx)
{
    int status = GR_SUCCESS;

    gr_ctx_struct *A_coeff_ctx = POLYNOMIAL_CTX(A_poly_ctx)->base_ring;
    gr_ctx_struct *sol_coeff_ctx = POLYNOMIAL_CTX(sol_poly_ctx)->base_ring;

    slong diff_order = gr_poly_length(A_scalarform, sol_coeff_ctx) - 1;

    gr_ctx_t A_mat_ctx;
    gr_ctx_init_matrix_ring(A_mat_ctx, A_coeff_ctx, diff_order);

    // NOTE: We create just one context if we can.
    gr_ctx_t sol_mat_ctx_tmp;
    gr_ctx_struct *sol_mat_ctx;
    if (A_poly_ctx == sol_poly_ctx) {
        sol_mat_ctx = A_mat_ctx;
    } else {
        gr_ctx_init_matrix_ring(sol_mat_ctx_tmp, sol_coeff_ctx, diff_order);
        sol_mat_ctx = sol_mat_ctx_tmp;
    }

    gr_poly_t A_numerator;
    gr_poly_init(A_numerator, A_mat_ctx);

    gr_poly_t A_denominator;
    gr_poly_init(A_denominator, A_coeff_ctx);

    status |= gr_mat_poly_from_scalar_diffop(A_numerator, A_denominator, A_scalarform, A_mat_ctx, A_poly_ctx);

    status |= test_gr_mat_poly_solve_ode(res, test_flags, out, A_numerator, A_denominator, t0, Y0, len, gr_mat_poly_solve_ode_method, method_name, method_flags, A_mat_ctx, sol_mat_ctx);

    gr_poly_clear(A_denominator, A_coeff_ctx);
    gr_poly_clear(A_numerator, A_mat_ctx);
    if (A_poly_ctx != sol_poly_ctx) {
        gr_ctx_clear(sol_mat_ctx_tmp);
    }
    gr_ctx_clear(A_mat_ctx);

    return status;
}

int test_gr_mat_poly_solve_ode(solve_ode_test_result_t res, int test_flags, gr_stream_t out, gr_poly_t A_numerator, gr_poly_t A_denominator, gr_ptr t0, gr_mat_t Y0, slong len, gr_mat_poly_solve_ode_method_t gr_mat_poly_solve_ode_method, char* method_name, int method_flags, gr_ctx_t A_mat_ctx, gr_ctx_t sol_mat_ctx)
{
    int status = GR_SUCCESS;

    gr_ctx_struct *A_coeff_ctx = MATRIX_CTX(A_mat_ctx)->base_ring;
    gr_ctx_struct *sol_coeff_ctx = MATRIX_CTX(sol_mat_ctx)->base_ring;

    slong diff_order = MATRIX_CTX(sol_mat_ctx)->nrows;

    gr_mat_t t0_mat;
    gr_mat_init(t0_mat, diff_order, diff_order, A_coeff_ctx);
    status |= gr_mat_set_scalar(t0_mat, t0, A_coeff_ctx);

    // Shift A_numerator and A_denominator.
    gr_poly_t A_numerator_shifted;
    gr_poly_init(A_numerator_shifted, A_mat_ctx);
    status |= gr_poly_taylor_shift(A_numerator_shifted, A_numerator, t0_mat, A_mat_ctx);
    gr_poly_t A_denominator_shifted;
    gr_poly_init(A_denominator_shifted, A_coeff_ctx);
    status |= gr_poly_taylor_shift(A_denominator_shifted, A_denominator, t0, A_coeff_ctx);

    if (status != GR_SUCCESS) {
        if (test_flags & SOLVE_ODE_TEST_WRITE_PROGRESS) {
            gr_stream_write(out, "Failed to construct input for ");
            gr_stream_write(out, method_name);
            gr_stream_write(out, ".\n");
        }
        return status;
    }

    gr_poly_t Y;
    gr_poly_init(Y, sol_mat_ctx);

    struct timespec start;
    clock_gettime(CLOCK_MONOTONIC, &start);

    status |= gr_mat_poly_solve_ode_method(Y, A_numerator_shifted, A_denominator_shifted, Y0, len, method_flags, A_mat_ctx, sol_mat_ctx);

    struct timespec finish;
    clock_gettime(CLOCK_MONOTONIC, &finish);

    res->time_elapsed_solving = finish.tv_sec - start.tv_sec;
    res->time_elapsed_solving += (finish.tv_nsec - start.tv_nsec) / 1e9;

    if (test_flags & SOLVE_ODE_TEST_WRITE_SOLUTION) {
        gr_stream_write(out, "Y = ");
        gr_poly_write(out, Y, "t", sol_mat_ctx);
        gr_stream_write(out, "\n\n");
    }

    // See how long it takes to square tr(Y).

    gr_poly_t trace_Y_squared;
    gr_poly_init(trace_Y_squared, sol_coeff_ctx);
    status |= gr_mat_poly_trace(trace_Y_squared, Y, sol_mat_ctx);

    if (test_flags & SOLVE_ODE_TEST_WRITE_SOLUTION) {
        gr_stream_write(out, "tr(Y) = ");
        gr_poly_write(out, trace_Y_squared, "t", sol_coeff_ctx);
        gr_stream_write(out, "\n\n");
    }

    clock_gettime(CLOCK_MONOTONIC, &start);

    status |= gr_poly_mul(trace_Y_squared, trace_Y_squared, trace_Y_squared, sol_coeff_ctx);

    clock_gettime(CLOCK_MONOTONIC, &finish);

    res->time_elapsed_squaring_trace = finish.tv_sec - start.tv_sec;
    res->time_elapsed_squaring_trace += (finish.tv_nsec - start.tv_nsec) / 1e9;

    int success = 1;

    // Test initial condition is satisfied.

    int ic_satisfied = 1;
    if (gr_poly_length(Y, sol_mat_ctx) == 0) {
        ic_satisfied = gr_mat_is_zero(Y0, sol_coeff_ctx) != T_FALSE;
    } else {
        ic_satisfied = gr_mat_equal(gr_poly_entry_ptr(Y, 0, sol_mat_ctx), Y0, sol_coeff_ctx) != T_FALSE;
    }
    if (!ic_satisfied) {
        success = 0;
    }
    res->initial_condition_satisfied = ic_satisfied;

    // Test by computing (A_denominator_shifted*Y' - A_numerator_shifted*Y) mod t^(len - 1).

    gr_poly_t error_term;
    gr_poly_init(error_term, sol_mat_ctx);

    gr_poly_t d;
    gr_poly_init(d, sol_mat_ctx);
    status |= gr_poly_set(d, Y, sol_mat_ctx);
    status |= gr_poly_derivative(d, d, sol_mat_ctx);
    if (A_mat_ctx == sol_mat_ctx) {
        status |= gr_mat_poly_mullow_poly(d, d, A_denominator_shifted, len - 1, sol_mat_ctx);
        status |= gr_poly_mullow(error_term, A_numerator_shifted, Y, len - 1, sol_mat_ctx);
    } else {
        gr_poly_t tmp_poly;
        gr_poly_init(tmp_poly, sol_coeff_ctx);
        status |= gr_poly_set_gr_poly_other(tmp_poly, A_denominator_shifted, A_coeff_ctx, sol_coeff_ctx);
        status |= gr_mat_poly_mullow_poly(d, d, tmp_poly, len - 1, sol_mat_ctx);
        gr_poly_clear(tmp_poly, sol_coeff_ctx);
        status |= gr_poly_set_gr_poly_other(error_term, A_numerator_shifted, A_mat_ctx, sol_mat_ctx);
        status |= gr_poly_mullow(error_term, error_term, Y, len - 1, sol_mat_ctx);
    }

    status |= gr_poly_sub(error_term, d, error_term, sol_mat_ctx);

    res->equation_satisfied = 1;
    if (gr_poly_is_zero(error_term, sol_mat_ctx) == T_FALSE) {
        res->equation_satisfied = 0;
        success = 0;
    }

    if (sol_coeff_ctx->which_ring == GR_CTX_CC_ACB) {
        res->radius_max_exp = acb_mat_poly_radius_max_exp(Y, sol_mat_ctx);
        if (res->radius_max_exp > 0) {
            success = 0;
        }
    }

    if (test_flags & SOLVE_ODE_TEST_WRITE_PROGRESS) {
        gr_stream_write(out, method_name);

        if (method_flags != 0) {
            gr_stream_write(out, " (");
            int method_flags_unseen = method_flags;
            int flag = 1;
            for (int i = 0; i < SOLVE_ODE_METHOD_FLAG_COUNT; i++) {
                if (method_flags_unseen & flag) {
                    gr_stream_write(out, solve_ode_method_flag_descriptions[i]);
                    method_flags_unseen &= ~flag;
                    if (method_flags_unseen != 0) {
                        gr_stream_write(out, ", ");
                    }
                }
                flag <<= 1;
            }
            gr_stream_write(out, ")");
        }

        if (success) {
            gr_stream_write(out, " succeeded.\n");
        } else {
            gr_stream_write(out, " failed.\n");
        }

        if (res->equation_satisfied == 0) {
            gr_stream_write(out, "- Error: The solution does not satisfy the original equation.\n");
        }

        if (res->initial_condition_satisfied == 0) {
            gr_stream_write(out, "- Error: The solution does not satisfy the initial condition.\n");
        }

        if (sol_coeff_ctx->which_ring == GR_CTX_CC_ACB) {
            gr_stream_write(out, "- The solution contains polynomials having coefficients with radius exponent <= ");
            gr_stream_write_fmpz(out, &res->radius_max_exp);
            gr_stream_write(out, ".\n");
        }

        gr_stream_write(out, "- Time to solve for Y: ");
        char time_elapsed_str[128];
        sprintf(time_elapsed_str, "%lf", res->time_elapsed_solving);
        gr_stream_write(out, time_elapsed_str);
        gr_stream_write(out, " seconds\n");

        gr_stream_write(out, "- Time to square tr(Y): ");
        sprintf(time_elapsed_str, "%lf", res->time_elapsed_squaring_trace);
        gr_stream_write(out, time_elapsed_str);
        gr_stream_write(out, " seconds\n");
    }

    gr_poly_clear(error_term, sol_mat_ctx);
    gr_poly_clear(d, sol_mat_ctx);
    gr_poly_clear(trace_Y_squared, sol_coeff_ctx);
    gr_poly_clear(Y, sol_mat_ctx);
    gr_poly_clear(A_denominator_shifted, A_coeff_ctx);
    gr_poly_clear(A_numerator_shifted, A_mat_ctx);
    gr_mat_clear(t0_mat, A_coeff_ctx);

    return status;
}

fmpz acb_poly_mat_radius_max_exp(gr_mat_t A, gr_ctx_t poly_ctx)
{
    gr_ctx_struct *coeff_ctx = POLYNOMIAL_CTX(poly_ctx)->base_ring;
    fmpz max_exp = COEFF_MIN;
    for (int i = 0; i < A->r; i++) {
        for (int j = 0; j < A->c; j++) {
            gr_ptr entry = gr_mat_entry_ptr(A, i, j, poly_ctx);
            for (int k = 0; k < gr_poly_length(entry, coeff_ctx); k++) {
                acb_ptr coeff = (acb_ptr)gr_poly_entry_ptr(entry, k, coeff_ctx);
                // NOTE: Exact coefficients are skipped from the maximum radius computation.
                if (acb_is_exact(coeff)) {
                    continue;
                }
                arb_ptr coeff_real = (arb_ptr)acb_realref(coeff);
                if (arb_is_zero(coeff_real) == 0) { // Nonzero real part.
                    mag_ptr coeff_real_rad = (mag_ptr)arb_radref(coeff_real);
                    if (fmpz_cmp(&max_exp, &coeff_real_rad->exp) < 0) {
                        fmpz_set(&max_exp, &coeff_real_rad->exp);
                    }
                }
                arb_ptr coeff_imag = (arb_ptr)acb_imagref(coeff);
                if (arb_is_zero(coeff_imag) == 0) { // Nonzero imaginary part.
                    mag_ptr coeff_imag_rad = (mag_ptr)arb_radref(coeff_imag);
                    if (fmpz_cmp(&max_exp, &coeff_imag_rad->exp) < 0) {
                        fmpz_set(&max_exp, &coeff_imag_rad->exp);
                    }
                }
            }
        }
    }
    return max_exp;
}

fmpz acb_mat_poly_radius_max_exp(gr_poly_t f, gr_ctx_t mat_ctx)
{
    gr_ctx_struct *coeff_ctx = MATRIX_CTX(mat_ctx)->base_ring;
    fmpz max_exp = COEFF_MIN;
    for (int k = 0; k < gr_poly_length(f, mat_ctx); k++) {
        gr_mat_struct *A = (gr_mat_struct*)gr_poly_entry_ptr(f, k, mat_ctx);
        for (int i = 0; i < A->r; i++) {
            for (int j = 0; j < A->c; j++) {
                acb_ptr coeff = gr_mat_entry_ptr(A, i, j, coeff_ctx);
                // NOTE: Exact coefficients are skipped from the maximum radius computation.
                if (acb_is_exact(coeff)) {
                    continue;
                }
                arb_ptr coeff_real = (arb_ptr)acb_realref(coeff);
                if (arb_is_zero(coeff_real) == 0) { // Nonzero real part.
                    mag_ptr coeff_real_rad = (mag_ptr)arb_radref(coeff_real);
                    if (fmpz_cmp(&max_exp, &coeff_real_rad->exp) < 0) {
                        fmpz_set(&max_exp, &coeff_real_rad->exp);
                    }
                }
                arb_ptr coeff_imag = (arb_ptr)acb_imagref(coeff);
                if (arb_is_zero(coeff_imag) == 0) { // Nonzero imaginary part.
                    mag_ptr coeff_imag_rad = (mag_ptr)arb_radref(coeff_imag);
                    if (fmpz_cmp(&max_exp, &coeff_imag_rad->exp) < 0) {
                        fmpz_set(&max_exp, &coeff_imag_rad->exp);
                    }
                }
            }
        }
    }
    return max_exp;
}
