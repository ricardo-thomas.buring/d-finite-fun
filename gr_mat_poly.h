#ifndef GR_MAT_POLY_H
#define GR_MAT_POLY_H

#include <flint/gr.h>
#include <flint/gr_poly.h>

WARN_UNUSED_RESULT int gr_mat_poly_mullow_poly(gr_poly_t result, gr_poly_t A, gr_poly_t f, slong len, gr_ctx_t mat_ctx);
WARN_UNUSED_RESULT int gr_mat_poly_trace(gr_poly_t result, gr_poly_t poly, gr_ctx_t mat_ctx);

WARN_UNUSED_RESULT int gr_mat_poly_set_gr_poly_mat(gr_poly_t result, gr_mat_t A, gr_ctx_t poly_ctx, gr_ctx_t mat_ctx);

#endif // GR_MAT_POLY_H
