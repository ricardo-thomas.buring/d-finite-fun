CFLAGS=-I$(HOME)/include/ -g -Wall
LDFLAGS=-L$(HOME)/lib/ -lflint

.PHONY: all
all: libd-finite-fun_flint.so tests

libd-finite-fun_flint.so: differential_operator_flint.o gr_mat_poly.o newton_iteration_flint.o divide_and_conquer_flint.o
	$(CC) -shared -o libd-finite-fun_flint.so differential_operator_flint.o gr_mat_poly.o newton_iteration_flint.o divide_and_conquer_flint.o $(LDFLAGS)

differential_operator_flint.o: differential_operator_flint.c
	$(CC) -o differential_operator_flint.o -c differential_operator_flint.c $(CFLAGS) -fPIC

gr_mat_poly.o: gr_mat_poly.c
	$(CC) -o gr_mat_poly.o -c gr_mat_poly.c $(CFLAGS) -fPIC

newton_iteration_flint.o: newton_iteration_flint.c
	$(CC) -o newton_iteration_flint.o -c newton_iteration_flint.c $(CFLAGS) -fPIC

divide_and_conquer_flint.o: divide_and_conquer_flint.c
	$(CC) -o divide_and_conquer_flint.o -c divide_and_conquer_flint.c $(CFLAGS) -fPIC

test_utils_flint.o: test_utils_flint.c
	$(CC) -o test_utils_flint.o -c test_utils_flint.c $(CFLAGS)

.PHONY: tests
tests: tests/test_newton_iteration_flint tests/test_divide_and_conquer_flint tests/benchmark_flint

tests/%: tests/%.o test_utils_flint.o libd-finite-fun_flint.so
	$(CC) -o $@ $< test_utils_flint.o -Wl,-rpath,. -L. -ld-finite-fun_flint $(LDFLAGS)

tests/%.o: tests/%.c
	$(CC) -o $@ -c $< $(CFLAGS)

.PHONY: clean
clean:
	rm -f *.o *.so tests/*.o tests/test_newton_iteration_flint tests/test_divide_and_conquer_flint tests/benchmark_flint
