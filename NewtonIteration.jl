using Nemo

# This is a Julia/Nemo implementation of the algorithm described in:
# Fast computation of power series solutions of systems of differential equations
# by A. Bostan, F. Chyzak, F. Ollivier, B. Salvy, É. Schost and A. Sedoglavic
# in Proceedings of the Eighteenth Annual ACM-SIAM Symposium on Discrete Algorithms, 2007, pp. 1012 -- 1021.


function power_series_inverse(poly, order)
    poly_series = abs_series(base_ring(poly), [coeff(poly, k) for k in 0:order], order+1, order+1)
    poly_inv = inv(poly_series)
    R = parent(poly)
    return R([coeff(poly_inv, k) for k in 0:order])
end


# Matrices with polynomial entries. Pre-computed denominator inverse.


function poly_mat_solve_ode_newton_preinv(A_numerator, A_denominator, Y0, order::Integer)
    R = base_ring(A_numerator)
    r = size(A_numerator, 1)
    A_denominator_inv = power_series_inverse(A_denominator, order)
    A_denominator_inv0 = subst(A_denominator_inv, 0)
    A0 = A_denominator_inv0 * change_base_ring(R, subst.(A_numerator, 0))
    I = identity_matrix(R, r)
    Y = (I + shift_left.(A0, 1)) * change_base_ring(R, Y0)
    Z = change_base_ring(R, inv(Y0))
    m = 2
    while m <= order/2
        Z = Z + truncate.(Z*(I - Y*Z), m)
        A_truncated = truncate.(truncate(A_denominator_inv, 2*m-1) * A_numerator, 2*m-1)
        d = derivative.(Y) - A_truncated*Y
        Y = Y - truncate.(Y*integral.(Z*d), 2*m)
        m = 2*m
    end
    return Y
end


# Matrices with polynomial entries. Iteration for denominator inverse.


function poly_mat_solve_ode_newton(A_numerator, A_denominator, Y0, order::Integer)
    R = base_ring(A_numerator)
    r = size(A_numerator, 1)
    A_denominator_inv0 = inv(subst(A_denominator, 0))
    A0 = A_denominator_inv0 * change_base_ring(R, subst.(A_numerator, 0))
    I = identity_matrix(R, r)
    Y = (I + shift_left.(A0, 1)) * change_base_ring(R, Y0)
    Z = change_base_ring(R, inv(Y0))
    m = 2
    # One initial iteration for A_denominator_inv mod z^2.
    tmp = truncate(A_denominator_inv0 * A_denominator, 2)
    tmp = shift_right(tmp, 1)
    tmp = shift_left(truncate(tmp * A_denominator_inv0, 1), 1)
    A_denominator_inv = A_denominator_inv0 - tmp
    while m <= order/2
        Z = Z + truncate.(Z*(I - Y*Z), m)

        # Iteration for A_denominator_inv mod z^(2*m).
        tmp = truncate(A_denominator_inv * truncate(A_denominator, 2*m), 2*m)
        tmp = shift_right(tmp, m)
        tmp = shift_left(truncate(tmp * A_denominator_inv, m), m)
        A_denominator_inv = A_denominator_inv - tmp

        A_truncated = truncate.(truncate(A_denominator_inv, 2*m-1) * A_numerator, 2*m-1)
        d = derivative.(Y) - A_truncated*Y
        Y = Y - truncate.(Y*integral.(Z*d), 2*m)
        m = 2*m
    end
    return Y
end


# Polynomials with matrix coefficients. Pre-computed denominator inverse.


function mat_poly_solve_ode_newton_preinv(A_numerator, A_denominator, Y0, order::Integer)
    R = parent(A_numerator)
    MR = base_ring(R)
    r = degree(MR)
    A_denominator_inv0 = inv(coeff(A_denominator, 0))
    A0 = R(A_denominator_inv0 * evaluate(A_numerator, 0))
    I = one(R)
    Y = (I + shift_left(A0, 1)) * R(Y0)
    Z = R(inv(Y0))
    A_denominator_inv = power_series_inverse(A_denominator, order)
    m = 2
    while m <= order/2
        Z = Z + truncate(Z*(I - Y*Z), m)
        A_denominator_inv_truncated = truncate(A_denominator_inv, 2*m - 1)
        A_truncated = truncate(mat_poly_mul_poly(A_numerator, A_denominator_inv_truncated), 2*m-1)
        d = derivative(Y) - A_truncated*Y
        Y = Y - truncate(Y*mat_poly_integral(Z*d), 2*m)
        m = 2*m
    end
    return Y
end


# Polynomials with matrix coefficients. Iteration for denominator inverse.


function mat_poly_solve_ode_newton(A_numerator, A_denominator, Y0, order::Integer)
    R = parent(A_numerator)
    MR = base_ring(R)
    r = degree(MR)
    A_denominator_inv0 = inv(coeff(A_denominator, 0))
    A0 = R(A_denominator_inv0 * evaluate(A_numerator, 0))
    I = one(R)
    Y = (I + shift_left(A0, 1)) * R(Y0)
    Z = R(inv(Y0))
    m = 2
    # One initial iteration for A_denominator_inv mod z^2.
    tmp = truncate(A_denominator_inv0 * A_denominator, 2)
    tmp = shift_right(tmp, 1)
    tmp = shift_left(truncate(tmp * A_denominator_inv0, 1), 1)
    A_denominator_inv = A_denominator_inv0 - tmp
    while m <= order/2
        Z = Z + truncate(Z*(I - Y*Z), m)

        # Iteration for A_denominator_inv mod z^(2*m).
        tmp = truncate(A_denominator_inv * truncate(A_denominator, 2*m), 2*m)
        tmp = shift_right(tmp, m)
        tmp = shift_left(truncate(tmp * A_denominator_inv, m), m)
        A_denominator_inv = A_denominator_inv - tmp

        A_truncated = truncate(mat_poly_mul_poly(A_numerator, truncate(A_denominator_inv, 2*m-1)), 2*m-1)
        d = derivative(Y) - A_truncated*Y
        Y = Y - truncate(Y*mat_poly_integral(Z*d), 2*m)
        m = 2*m
    end
    return Y
end
