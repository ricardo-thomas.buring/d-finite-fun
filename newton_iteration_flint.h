#ifndef NEWTON_ITERATION_FLINT_H
#define NEWTON_ITERATION_FLINT_H

#include <flint/gr.h>
#include <flint/gr_poly.h>
#include <flint/gr_mat.h>

WARN_UNUSED_RESULT int _gr_poly_mat_newton_iteration_start(gr_mat_t Y, gr_mat_t Z, gr_poly_t A_denominator_inv, gr_mat_t A_numerator, gr_poly_t A_denominator, gr_mat_t Y0, slong len, int method_flags, gr_ctx_t A_poly_ctx, gr_ctx_t sol_poly_ctx);
WARN_UNUSED_RESULT int _gr_poly_mat_newton_iteration_step(gr_mat_t Y, gr_mat_t Z, gr_poly_t A_denominator_inv, slong *len, gr_mat_t A_numerator, gr_poly_t A_denominator, int method_flags, gr_ctx_t A_poly_ctx, gr_ctx_t sol_poly_ctx);
WARN_UNUSED_RESULT int gr_poly_mat_solve_ode_newton(gr_mat_t Y, gr_mat_t A_numerator, gr_poly_t A_denominator, gr_mat_t Y0, slong len, int method_flags, gr_ctx_t A_poly_ctx, gr_ctx_t sol_poly_ctx);

WARN_UNUSED_RESULT int _gr_mat_poly_newton_iteration_start(gr_poly_t Y, gr_poly_t Z, gr_poly_t A_denominator_inv, gr_poly_t A_numerator, gr_poly_t A_denominator, gr_mat_t Y0, slong len, int method_flags, gr_ctx_t A_mat_ctx, gr_ctx_t sol_mat_ctx);
WARN_UNUSED_RESULT int _gr_mat_poly_newton_iteration_step(gr_poly_t Y, gr_poly_t Z, gr_poly_t A_denominator_inv, slong *len, gr_poly_t A_numerator, gr_poly_t A_denominator, int method_flags, gr_ctx_t A_mat_ctx, gr_ctx_t sol_mat_ctx);
WARN_UNUSED_RESULT int gr_mat_poly_solve_ode_newton(gr_poly_t Y, gr_poly_t A_numerator, gr_poly_t A_denominator, gr_mat_t Y0, slong len, int method_flags, gr_ctx_t A_mat_poly_ctx, gr_ctx_t sol_mat_poly_ctx);

#endif // NEWTON_ITERATION_FLINT_H
