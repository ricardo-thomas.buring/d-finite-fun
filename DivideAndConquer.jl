using Nemo

# This is a Julia/Nemo implementation of the divide and conquer algorithm described in:
# Fast computation of power series solutions of systems of differential equations
# by A. Bostan, F. Chyzak, F. Ollivier, B. Salvy, É. Schost and A. Sedoglavic
# in Proceedings of the Eighteenth Annual ACM-SIAM Symposium on Discrete Algorithms, 2007, pp. 1012 -- 1021.

# Compared with the algorithm cited above, the single solution vector is replaced by a matrix of solutions.
# Also, in the first recursive call to obtain the lower part of the solution we truncate the matrices A and S.


function power_series_inverse(poly, order)
    poly_series = abs_series(base_ring(poly), [coeff(poly, k) for k in 0:order], order+1, order+1)
    poly_inv = inv(poly_series)
    R = parent(poly)
    return R([coeff(poly_inv, k) for k in 0:order])
end


# Matrices with polynomial entries. Pre-computed denominator inverse.


function poly_mat_divide_and_conquer_preinv(A_truncated1, S, p, m, V)
    R = base_ring(A_truncated1)
    if m == 1
        Y = (p == 0) ? V : inv(p) * change_base_ring(R, subst.(S, 0))
        @debug "poly_mat_divide_and_conquer_preinv" m p S Y
        return Y
    else
        d = div(m, 2)
        I = one(parent(A_truncated1))
        S0 = truncate.(S, d)
        A_truncated0 = truncate.(A_truncated1, d)
        Y0 = poly_mat_divide_and_conquer_preinv(A_truncated0, S0, p, d, V)
        S1 = S - shift_left.(derivative.(Y0), 1) - (p*I - shift_left.(A_truncated1, 1))*Y0
        S1 = shift_right.(truncate.(S1, m), d)
        @debug "poly_mat_divide_and_conquer_preinv" m p Y0 S A_truncated1 S1
        Y1 = poly_mat_divide_and_conquer_preinv(A_truncated1, S1, p + d, m - d, V)
        return Y0 + shift_left.(Y1, d)
    end
end


function poly_mat_solve_ode_divide_and_conquer_preinv(A_numerator, A_denominator, Y0, order::Integer)
    @debug "poly_mat_solve_ode_divide_and_conquer_preinv" A_numerator A_denominator Y0 order
    R = base_ring(A_numerator)
    S = parent(A_numerator)
    K = base_ring(R)
    A_denominator_inv = power_series_inverse(A_denominator, order)
    A_truncated1 = truncate.(A_denominator_inv*A_numerator, order)
    return poly_mat_divide_and_conquer_preinv(A_truncated1, zero(S), zero(K), order, change_base_ring(R, Y0))
end


# Matrices with polynomial entries. Iteration for denominator inverse.


function poly_mat_divide_and_conquer(A_numerator1, A_denominator, A_denominator_inv, A_denominator_inv_order, S, p, m, V)
    R = base_ring(A_numerator1)
    if m == 1
        Y = (p == 0) ? V : inv(p) * change_base_ring(R, subst.(S, 0))
        @debug "poly_mat_divide_and_conquer" m p S Y
        return Y
    else
        d = div(m, 2)
        I = one(parent(A_numerator1))
        S0 = truncate.(S, d)
        A_numerator0 = truncate.(A_numerator1, d)
        Y0 = poly_mat_divide_and_conquer(A_numerator0, A_denominator, A_denominator_inv, A_denominator_inv_order, S0, p, d, V)

        # NOTE: The order of the inverse of A_denominator may have already been doubled in the first branch at the same depth.
        if A_denominator_inv_order[] < m
            @assert A_denominator_inv_order[] * 2 == m

            # Iteration for A_denominator_inv mod z^m.
            tmp = truncate(A_denominator_inv[] * truncate(A_denominator, m), m)
            tmp = shift_right(tmp, d)
            tmp = shift_left(truncate(tmp * A_denominator_inv[], d), d)
            A_denominator_inv[] = A_denominator_inv[] - tmp

            A_denominator_inv_order[] *= 2
        end

        # TODO: Optimize the following.
        A_truncated1 = truncate.(A_denominator_inv[] * A_numerator1, m - 1)
        S1 = S - shift_left.(derivative.(Y0), 1) - (p*I - shift_left.(A_truncated1, 1))*Y0
        S1 = shift_right.(truncate.(S1, m), d)

        @debug "poly_mat_divide_and_conquer" m p Y0 S A_denominator_inv A_truncated1 S1

        Y1 = poly_mat_divide_and_conquer(A_numerator1, A_denominator, A_denominator_inv, A_denominator_inv_order, S1, p + d, m - d, V)
        return Y0 + shift_left.(Y1, d)
    end
end


function poly_mat_solve_ode_divide_and_conquer(A_numerator, A_denominator, Y0, order::Integer)
    @debug "poly_mat_solve_ode_divide_and_conquer" A_numerator A_denominator Y0 order
    R = base_ring(A_numerator)
    S = parent(A_numerator)
    K = base_ring(R)
    A_denominator_inv = Ref(R(inv(subst(A_denominator, 0))))
    A_denominator_inv_order = Ref(1)
    return poly_mat_divide_and_conquer(A_numerator, A_denominator, A_denominator_inv, A_denominator_inv_order, zero(S), zero(K), order, change_base_ring(R, Y0))
end


# Polynomials with matrix coefficients. Pre-computed denominator inverse.


function mat_poly_divide_and_conquer_preinv(A_truncated1, S, p, m, V)
    R = parent(A_truncated1)
    if m == 1
        Y = (p == 0) ? V : inv(p) * truncate(S, 1)
        @debug "mat_poly_divide_and_conquer_preinv" m p S Y
        return Y
    else
        d = div(m, 2)
        I = one(R)
        S0 = truncate(S, d)
        A_truncated0 = truncate(A_truncated1, d)
        Y0 = mat_poly_divide_and_conquer_preinv(A_truncated0, S0, p, d, V)
        S1 = S - shift_left(derivative(Y0), 1) - (p*I - shift_left(A_truncated1, 1))*Y0
        S1 = shift_right(truncate(S1, m), d)

        @debug "mat_poly_divide_and_conquer_preinv" m p Y0 S A_truncated1 S1

        Y1 = mat_poly_divide_and_conquer_preinv(A_truncated1, S1, p + d, m - d, V)

        return Y0 + shift_left(Y1, d)
    end
end


function mat_poly_solve_ode_divide_and_conquer_preinv(A_numerator, A_denominator, Y0, order::Integer)
    R = parent(A_numerator)
    A_denominator_inv = power_series_inverse(A_denominator, order)
    A_truncated1 = truncate(mat_poly_mul_poly(A_numerator, A_denominator_inv), order)
    return mat_poly_divide_and_conquer_preinv(A_truncated1, zero(R), zero(base_ring(A_denominator)), order, R(Y0))
end


# Polynomials with matrix coefficients. Iteration for denominator inverse.


function mat_poly_divide_and_conquer(A_numerator1, A_denominator, A_denominator_inv, A_denominator_inv_order, S, p, m, V)
    R = parent(A_numerator1)
    if m == 1
        Y = (p == 0) ? V : inv(p) * truncate(S, 1)
        @debug "mat_poly_divide_and_conquer" m p S Y
        return Y
    else
        d = div(m, 2)
        I = one(R)
        S0 = truncate(S, d)
        A_numerator0 = truncate(A_numerator1, d)
        Y0 = mat_poly_divide_and_conquer(A_numerator0, A_denominator, A_denominator_inv, A_denominator_inv_order, S0, p, d, V)

        # NOTE: The order of the inverse of A_denominator may have already been doubled in the first branch at the same depth.
        if A_denominator_inv_order[] < m
            @assert A_denominator_inv_order[] * 2 == m

            # Iteration for A_denominator_inv mod z^m.
            tmp = truncate(A_denominator_inv[] * truncate(A_denominator, m), m)
            tmp = shift_right(tmp, d)
            tmp = shift_left(truncate(tmp * A_denominator_inv[], d), d)
            A_denominator_inv[] = A_denominator_inv[] - tmp

            A_denominator_inv_order[] *= 2
        end

        # TODO: Optimize the following.
        A_truncated1 = truncate(mat_poly_mul_poly(A_numerator1, A_denominator_inv[]), m - 1)
        S1 = S - shift_left(derivative(Y0), 1) - (p*I - shift_left(A_truncated1, 1))*Y0
        S1 = shift_right(truncate(S1, m), d)

        @debug "mat_poly_divide_and_conquer" m p Y0 S A_denominator_inv A_truncated1 S1

        Y1 = mat_poly_divide_and_conquer(A_numerator1, A_denominator, A_denominator_inv, A_denominator_inv_order, S1, p + d, m - d, V)

        return Y0 + shift_left(Y1, d)
    end
end


function mat_poly_solve_ode_divide_and_conquer(A_numerator, A_denominator, Y0, order::Integer)
    @debug "mat_poly_solve_ode_divide_and_conquer" A_numerator A_denominator Y0 order
    R = parent(A_numerator)
    S = parent(A_denominator)
    K = base_ring(S)
    A_denominator_inv = Ref(S(inv(coeff(A_denominator, 0))))
    A_denominator_inv_order = Ref(1)
    return mat_poly_divide_and_conquer(A_numerator, A_denominator, A_denominator_inv, A_denominator_inv_order, zero(R), zero(K), order, R(Y0))
end
