using Nemo

# Matrices with polynomial entries.

function test_poly_mat_solve_ode(A_scalarform, t0, order, poly_mat_solve_ode_method, method_name)
    t = gen(base_ring(A_scalarform))

    A_numerator, A_denominator = poly_mat_from_scalar_diffop(A_scalarform)

    # Shift A_numerator and A_denominator
    A_numerator = poly_mat_taylor_shift(A_numerator, t0)
    A_denominator = subst(A_denominator, t + t0)

    Y0 = identity_matrix(C, size(A_numerator, 1))
    Y = poly_mat_solve_ode_method(A_numerator, A_denominator, Y0, order)
    println("Y = ", Y, "\n")

    error_term = A_denominator*derivative.(Y) - A_numerator*Y
    error_term = truncate.(error_term, order - 1)

    if poly_mat_contains_zero(error_term)
        println("$(method_name) succeeded.")
    else
        println("$(method_name) failed.")
    end
end

# Polynomials with matrix coefficients.

function test_mat_poly_solve_ode(A_scalarform, t0, order, mat_poly_solve_ode_method, method_name)
    t = gen(base_ring(A_scalarform))

    A_numerator, A_denominator = mat_poly_from_scalar_diffop(A_scalarform)

    # Shift A_numerator and A_denominator
    A_numerator = mat_poly_taylor_shift(A_numerator, t0)
    A_denominator = subst(A_denominator, t + t0)

    Y0 = one(base_ring(A_numerator))
    Y = mat_poly_solve_ode_method(A_numerator, A_denominator, Y0, order)
    println("Y = ", Y, "\n")

    error_term = mat_poly_mul_poly(derivative(Y), A_denominator) - A_numerator*Y
    error_term = truncate(error_term, order - 1)

    if mat_poly_contains_zero(error_term)
        println("$(method_name) succeeded.")
    else
        println("$(method_name) failed.")
    end
end
