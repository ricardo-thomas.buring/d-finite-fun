#include "newton_iteration_flint.h"

#include <flint/gr_vec.h>

#include "gr_mat_poly.h"
#include "solve_ode_method.h"

/*
 * This is a C/FLINT implementation of the algorithm described in:
 * Fast computation of power series solutions of systems of differential equations
 * by A. Bostan, F. Chyzak, F. Ollivier, B. Salvy, É. Schost and A. Sedoglavic
 * in Proceedings of the Eighteenth Annual ACM-SIAM Symposium on Discrete Algorithms, 2007, pp. 1012 -- 1021.
 */

// Compared with the algorithm cited above, we use an iteration for the inverse of the denominator of the matrix A.
// When A is a companion matrix, the polynomial matrix multiplication by A modulo z^m is optimized.

// This is a specialized implementation of univariate polynomial matrix multiplication
// for the case where A = (companion matrix) and the result is computed modulo z^m.
int gr_poly_mat_mullow_companion(gr_mat_t res, gr_mat_t A, gr_mat_t B, slong m, gr_ctx_t poly_ctx)
{
    int status = GR_SUCCESS;
    gr_ctx_struct *coeff_ctx = POLYNOMIAL_CTX(poly_ctx)->base_ring;
    gr_mat_t tmp_mat;
    gr_mat_init(tmp_mat, A->r, A->c, poly_ctx);
    status |= gr_mat_zero(tmp_mat, poly_ctx);
    for (int i = 0; i < A->r - 1; i++) {
        for (int j = 0; j < A->c; j++) {
            gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, i, j, poly_ctx);
            gr_ptr entry_B = gr_mat_entry_ptr(B, i + 1, j, poly_ctx);
            status |= gr_poly_set(entry_tmp, entry_B, coeff_ctx);
        }
    }
    gr_ptr tmp_poly;
    GR_TMP_INIT(tmp_poly, poly_ctx);
    for (int j = 0; j < A->c; j++) {
        gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, A->r - 1, j, poly_ctx);
        for (int i = 0; i < B->r; i++) {
            gr_ptr entry_A = gr_mat_entry_ptr(A, A->r - 1, i, poly_ctx);
            gr_ptr entry_B = gr_mat_entry_ptr(B, i, j, poly_ctx);
            // NOTE: Computing the product modulo z^m.
            status |= gr_poly_mullow(tmp_poly, entry_A, entry_B, m, coeff_ctx);
            status |= gr_poly_add(entry_tmp, entry_tmp, tmp_poly, coeff_ctx);
        }
    }
    GR_TMP_CLEAR(tmp_poly, poly_ctx);
    gr_mat_swap(res, tmp_mat, poly_ctx);
    gr_mat_clear(tmp_mat, poly_ctx);
    return status;
}

/* Matrices with polynomial entries. Iteration for denominator inverse. */

int _gr_poly_mat_newton_iteration_start(gr_mat_t Y, gr_mat_t Z, gr_poly_t A_denominator_inv, gr_mat_t A_numerator, gr_poly_t A_denominator, gr_mat_t Y0, slong len, int method_flags, gr_ctx_t A_poly_ctx, gr_ctx_t sol_poly_ctx) {
    gr_ctx_struct *A_coeff_ctx = POLYNOMIAL_CTX(A_poly_ctx)->base_ring;
    gr_ctx_struct *sol_coeff_ctx = POLYNOMIAL_CTX(sol_poly_ctx)->base_ring;

    int status = GR_SUCCESS;

    // Initialize A_denominator_inv
    if (method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) {
        // Invert the polynomial A_denominator as a power series.
        status |= gr_poly_inv_series(A_denominator_inv, A_denominator, len, sol_coeff_ctx);
    } else {
        // Calculate the first term of the power series inverse: 1 / A_denominator(0).
        status |= gr_poly_one(A_denominator_inv, sol_coeff_ctx);
        if (gr_poly_length(A_denominator, sol_coeff_ctx) > 0) {
            gr_ptr A_denominator0 = gr_poly_entry_ptr(A_denominator, 0, sol_coeff_ctx);
            status |= gr_poly_div_scalar(A_denominator_inv, A_denominator_inv, A_denominator0, sol_coeff_ctx);
        } else {
            status = GR_DOMAIN;
        }
    }

    if (status != GR_SUCCESS) {
        return status;
    }

    gr_poly_t tmp_poly;
    gr_poly_init(tmp_poly, sol_coeff_ctx);

    gr_mat_t tmp_mat;
    gr_mat_init(tmp_mat, Y->r, Y->c, sol_poly_ctx);

    // Next we are going to set Y = (I + A(0)*z)*Y0 in a few steps.

    // Y = A(0)*z
    for (int i = 0; i < Y->r; i++) {
        for (int j = 0; j < Y->c; j++) {
            gr_ptr entry_A = gr_mat_entry_ptr(A_numerator, i, j, sol_poly_ctx);
            gr_ptr entry_Y = gr_mat_entry_ptr(Y, i, j, sol_poly_ctx);
            // TODO: Add constant coefficient of A to Y instead of setting and truncating.
            if (A_poly_ctx == sol_poly_ctx) {
                status |= gr_poly_set(entry_Y, entry_A, sol_coeff_ctx);
            } else {
                status |= gr_poly_set_gr_poly_other(entry_Y, entry_A, A_coeff_ctx, sol_coeff_ctx);
            }
            status |= gr_poly_truncate(entry_Y, entry_Y, 1, sol_coeff_ctx);
            status |= gr_poly_shift_left(entry_Y, entry_Y, 1, sol_coeff_ctx);
        }
    }
    status |= gr_mat_mul_scalar(Y, Y, A_denominator_inv, sol_poly_ctx);

    // Y = (I + Y)*Y0
    status |= gr_poly_one(tmp_poly, sol_coeff_ctx);
    status |= gr_mat_add_scalar(Y, Y, tmp_poly, sol_poly_ctx);
    status |= gr_mat_set_gr_mat_other(tmp_mat, Y0, sol_coeff_ctx, sol_poly_ctx);
    status |= gr_mat_mul(Y, Y, tmp_mat, sol_poly_ctx);

    // Z = Y0^{-1}
    status |= gr_mat_inv(Z, tmp_mat, sol_poly_ctx);

    if (method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) {
        // TODO: Don't modify the input?
        // FIXME: This code assumes A_coeff_ctx == sol_coeff_ctx

        // Replace A_numerator by the power series expansion of A_numerator / A_denominator.
        for (int i = 0; i < A_numerator->r; i++) {
            for (int j = 0; j < A_numerator->c; j++) {
                gr_ptr entry_A = gr_mat_entry_ptr(A_numerator, i, j, sol_poly_ctx);
                status |= gr_poly_mullow(entry_A, entry_A, A_denominator_inv, len, sol_coeff_ctx);
            }
        }
        // Replace A_denominator by one.
        status |= gr_poly_one(A_denominator, A_coeff_ctx);
        status |= gr_poly_one(A_denominator_inv, sol_coeff_ctx);
    } else {
        // One initial iteration for A_denominator_inv mod z^2.
        int m = 2;
        status |= gr_poly_truncate(tmp_poly, A_denominator, m, sol_coeff_ctx);
        status |= gr_poly_mullow(tmp_poly, tmp_poly, A_denominator_inv, m, sol_coeff_ctx);
        status |= gr_poly_shift_right(tmp_poly, tmp_poly, m / 2, sol_coeff_ctx);
        status |= gr_poly_mullow(tmp_poly, tmp_poly, A_denominator_inv, m / 2, sol_coeff_ctx);
        status |= gr_poly_shift_left(tmp_poly, tmp_poly, m / 2, sol_coeff_ctx);
        status |= gr_poly_sub(A_denominator_inv, A_denominator_inv, tmp_poly, sol_coeff_ctx);
    }

    gr_mat_clear(tmp_mat, sol_poly_ctx);
    gr_poly_clear(tmp_poly, sol_coeff_ctx);

    return status;
}

int _gr_poly_mat_newton_iteration_step(gr_mat_t Y, gr_mat_t Z, gr_poly_t A_denominator_inv, slong *len, gr_mat_t A_numerator, gr_poly_t A_denominator, int method_flags, gr_ctx_t A_poly_ctx, gr_ctx_t sol_poly_ctx) {
    gr_ctx_struct *sol_coeff_ctx = POLYNOMIAL_CTX(sol_poly_ctx)->base_ring;

    gr_poly_t tmp_poly;
    gr_poly_init(tmp_poly, sol_coeff_ctx);

    gr_mat_t tmp_mat;
    gr_mat_init(tmp_mat, Y->r, Y->c, sol_poly_ctx);

    slong m = *len;

    int status = GR_SUCCESS;

    // Z = Z + (Z*(I - Y*Z) mod z^m)
    status |= gr_poly_one(tmp_poly, sol_coeff_ctx);
    status |= gr_mat_mul(tmp_mat, Y, Z, sol_poly_ctx);
    status |= gr_mat_sub_scalar(tmp_mat, tmp_mat, tmp_poly, sol_poly_ctx);
    status |= gr_mat_mul(tmp_mat, Z, tmp_mat, sol_poly_ctx);
    for (int i = 0; i < tmp_mat->r; i++) {
        for (int j = 0; j < tmp_mat->c; j++) {
            gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, i, j, sol_poly_ctx);
            status |= gr_poly_truncate(entry_tmp, entry_tmp, m, sol_coeff_ctx);
        }
    }
    status |= gr_mat_sub(Z, Z, tmp_mat, sol_poly_ctx);

    if ((method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) == 0) {
        // Iteration for A_denominator_inv mod z^{2*m}.
        status |= gr_poly_truncate(tmp_poly, A_denominator, 2*m, sol_coeff_ctx);
        // TODO: Use middle product here?
        status |= gr_poly_mullow(tmp_poly, tmp_poly, A_denominator_inv, 2*m, sol_coeff_ctx);
        status |= gr_poly_shift_right(tmp_poly, tmp_poly, m, sol_coeff_ctx);
        status |= gr_poly_mullow(tmp_poly, tmp_poly, A_denominator_inv, m, sol_coeff_ctx);
        status |= gr_poly_shift_left(tmp_poly, tmp_poly, m, sol_coeff_ctx);
        status |= gr_poly_sub(A_denominator_inv, A_denominator_inv, tmp_poly, sol_coeff_ctx);
    }

    // Next we are going to set d = Y' - (A mod z^{2m - 1})*Y mod z^{2m - 1} in a few steps.

    // tmp_mat = (A mod z^{2m - 1})*Y
    // TODO: Optimize truncation of A.
    if (A_poly_ctx == sol_poly_ctx) {
        status |= gr_mat_set(tmp_mat, A_numerator, sol_poly_ctx);
    } else {
        // TODO: Don't redo the same conversion all the time.
        status |= gr_mat_set_gr_mat_other(tmp_mat, A_numerator, A_poly_ctx, sol_poly_ctx);
    }
    if (method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) {
        // TODO: If A_numerator is the full series then the gr_mat_set above is probably excessive in most cases.
        for (int i = 0; i < Y->r; i++) {
            for (int j = 0; j < Y->c; j++) {
                gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, i, j, sol_poly_ctx);
                status |= gr_poly_truncate(entry_tmp, entry_tmp, 2*m - 1, sol_coeff_ctx);
            }
        }
    } else {
        for (int i = 0; i < Y->r; i++) {
            for (int j = 0; j < Y->c; j++) {
                gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, i, j, sol_poly_ctx);
                status |= gr_poly_mullow(entry_tmp, entry_tmp, A_denominator_inv, 2*m - 1, sol_coeff_ctx);
            }
        }
    }

    if (method_flags & SOLVE_ODE_METHOD_ASSUME_A_IS_COMPANION) {
        status |= gr_poly_mat_mullow_companion(tmp_mat, tmp_mat, Y, 2*m - 1, sol_poly_ctx);
    } else {
        status |= gr_mat_mul(tmp_mat, tmp_mat, Y, sol_poly_ctx);
    }

    // d = Y' - tmp_mat
    gr_mat_t d;
    gr_mat_init(d, Y->r, Y->c, sol_poly_ctx);
    // TODO: Implement gr_poly_mat_derivative?
    status |= gr_mat_set(d, Y, sol_poly_ctx);
    for (int i = 0; i < Y->r; i++) {
        for (int j = 0; j < Y->c; j++) {
            gr_ptr entry_d = gr_mat_entry_ptr(d, i, j, sol_poly_ctx);
            status |= gr_poly_derivative(entry_d, entry_d, sol_coeff_ctx);
        }
    }
    status |= gr_mat_sub(d, d, tmp_mat, sol_poly_ctx);

    // Y = Y - (Y*integral(Z*d) mod z^{2m})
    status |= gr_mat_mul(tmp_mat, Z, d, sol_poly_ctx);
    // TODO: Implement gr_poly_mat_integral?
    for (int i = 0; i < Y->r; i++) {
        for (int j = 0; j < Y->c; j++) {
            gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, i, j, sol_poly_ctx);
            status |= gr_poly_integral(entry_tmp, entry_tmp, sol_coeff_ctx);
        }
    }
    status |= gr_mat_mul(tmp_mat, Y, tmp_mat, sol_poly_ctx);
    for (int i = 0; i < tmp_mat->r; i++) {
        for (int j = 0; j < tmp_mat->c; j++) {
            gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, i, j, sol_poly_ctx);
            status |= gr_poly_truncate(entry_tmp, entry_tmp, 2*m, sol_coeff_ctx);
        }
    }
    status |= gr_mat_sub(Y, Y, tmp_mat, sol_poly_ctx);

    *len *= 2;

    gr_mat_clear(d, sol_poly_ctx);
    gr_mat_clear(tmp_mat, sol_poly_ctx);
    gr_poly_clear(tmp_poly, sol_coeff_ctx);

    return status;
}

int gr_poly_mat_solve_ode_newton(gr_mat_t Y, gr_mat_t A_numerator, gr_poly_t A_denominator, gr_mat_t Y0, slong len, int method_flags, gr_ctx_t A_poly_ctx, gr_ctx_t sol_poly_ctx)
{
    gr_ctx_struct *A_coeff_ctx = POLYNOMIAL_CTX(A_poly_ctx)->base_ring;
    gr_ctx_struct *sol_coeff_ctx = POLYNOMIAL_CTX(sol_poly_ctx)->base_ring;

    int status = GR_SUCCESS;

    // Convert A_denominator into the solution context.
    gr_poly_t A_denominator_sol;
    gr_poly_init(A_denominator_sol, sol_coeff_ctx);
    if (A_poly_ctx == sol_poly_ctx) {
        status |= gr_poly_set(A_denominator_sol, A_denominator, sol_coeff_ctx);
    } else {
        status |= gr_poly_set_gr_poly_other(A_denominator_sol, A_denominator, A_coeff_ctx, sol_coeff_ctx);
    }

    // TODO: Don't modify the input?
    if (method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) {
        status |= gr_poly_one(A_denominator, A_coeff_ctx);
    }

    // The power series expansion of the inverse of A_denominator.
    gr_poly_t A_denominator_inv;
    gr_poly_init(A_denominator_inv, sol_coeff_ctx);

    // The power series expansion of the inverse matrix Z of the matrix Y.
    gr_mat_t Z;
    gr_mat_init(Z, Y->r, Y->c, sol_poly_ctx);

    slong m = 2;
    status |= _gr_poly_mat_newton_iteration_start(Y, Z, A_denominator_inv, A_numerator, A_denominator_sol, Y0, len, method_flags, A_poly_ctx, sol_poly_ctx);
    while (m <= len / 2) {
        status |= _gr_poly_mat_newton_iteration_step(Y, Z, A_denominator_inv, &m, A_numerator, A_denominator_sol, method_flags, A_poly_ctx, sol_poly_ctx);
    }

    // TODO: Add a way to retrieve the inverse Z.
    gr_mat_clear(Z, sol_poly_ctx);
    gr_poly_clear(A_denominator_inv, sol_coeff_ctx);
    gr_poly_clear(A_denominator_sol, sol_coeff_ctx);

    return status;
}

/* Polynomials with matrix coefficients. Iteration for denominator inverse. */

// This is a specialized implementation of univariate polynomial matrix multiplication
// for the case where A = (companion matrix) and the result is computed modulo z^m.
int gr_mat_poly_mullow_companion(gr_poly_t res, gr_poly_t A, gr_poly_t B, slong m, gr_ctx_t mat_ctx)
{
    gr_ctx_struct *coeff_ctx = MATRIX_CTX(mat_ctx)->base_ring;

    int status = GR_SUCCESS;

    // [ 0 1 0 0 ]   [ b11 b12 b13 b14 ] = [ b21 b22 b23 b24 ]
    // [ 0 0 1 0 ] * [ b21 b22 b23 b24 ]   [ b31 b32 b33 b34 ]
    // [ 0 0 0 1 ]   [ b31 b31 b33 b34 ]   [ b41 b42 b43 b44 ]
    // [ p q r s ]   [ b41 b42 b43 b44 ]   [ ... ... ... ... ]

    // TODO: Make this actually faster than gr_poly_mullow.

    gr_poly_t tmp_poly;
    gr_poly_init(tmp_poly, mat_ctx);

    // tmp_poly = B mod z^m
    status |= gr_poly_truncate(tmp_poly, B, m, mat_ctx);

    slong len_tmp = gr_poly_length(tmp_poly, mat_ctx);

    // deg(A*B) = deg(A) + deg(B) => length(A*B) = length(A) + length(B) - 1
    // TODO: Handle length(A) = length(B) = 0?
    slong newlen = FLINT_MIN(A->length + B->length - 1, m);
    gr_poly_fit_length(tmp_poly, newlen, mat_ctx);
    _gr_poly_set_length(tmp_poly, newlen, mat_ctx);
    // TODO: Avoid zeroing?
    slong sz = mat_ctx->sizeof_elem;
    status |= _gr_vec_zero(GR_ENTRY(tmp_poly->coeffs, len_tmp, sz), newlen - len_tmp, mat_ctx);

    // Next we are going to compute tmp_poly = A*B in a few steps.

    slong len_A = gr_poly_length(A, mat_ctx);
    slong len_B = gr_poly_length(B, mat_ctx);

    for (slong k = 0; k < newlen; k++) {
        gr_mat_struct *tmp_mat = (gr_mat_struct*)gr_poly_entry_ptr(tmp_poly, k, mat_ctx);
        // Set the top block of tmp_poly directly (all but the bottom row).
        for (slong i = 0; i < tmp_mat->r - 1; i++) {
            for (slong j = 0; j < tmp_mat->c; j++) {
                gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, i, j, coeff_ctx);
                gr_ptr entry_tmp_below = gr_mat_entry_ptr(tmp_mat, i + 1, j, coeff_ctx);
                status |= gr_set(entry_tmp, entry_tmp_below, coeff_ctx);
            }
        }
        // Zero out the bottom row.
        for (slong j = 0; j < tmp_mat->c; j++) {
            gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, tmp_mat->r - 1, j, coeff_ctx);
            status |= gr_zero(entry_tmp, coeff_ctx);
        }
    }

    // Add the dot products in the bottom row.
    for (slong k = 0; k < newlen; k++) {
        gr_mat_struct *tmp_mat = (gr_mat_struct*)gr_poly_entry_ptr(tmp_poly, k, mat_ctx);
        for (slong k1 = 0; k1 < FLINT_MIN(len_A, k + 1); k1++) {
            gr_mat_struct *A_mat = (gr_mat_struct*)gr_poly_entry_ptr(A, k1, mat_ctx);
            slong k2 = k - k1;
            if (k2 >= len_B) {
                continue;
            }
            gr_mat_struct *B_mat = (gr_mat_struct*)gr_poly_entry_ptr(B, k2, mat_ctx);
            for (slong j = 0; j < A_mat->c; j++) {
                // In the jth column of the bottom row of the tmp_mat...
                gr_ptr entry_tmp = gr_mat_entry_ptr(tmp_mat, tmp_mat->r - 1, j, coeff_ctx);
                // ...add the dot product of the last row of A with the jth column of B.
                for (slong i = 0; i < B_mat->r; i++) {
                    gr_ptr entry_A = gr_mat_entry_ptr(A_mat, A_mat->r - 1, i, coeff_ctx);
                    gr_ptr entry_B = gr_mat_entry_ptr(B_mat, i, j, coeff_ctx);
                    status |= gr_addmul(entry_tmp, entry_A, entry_B, coeff_ctx);
                }
            }
        }
    }

    // TODO: Necessary?
    _gr_poly_normalise(tmp_poly, mat_ctx);

    // TODO: Do the whole operation in-place instead.
    // (using a temporary vector for a copy of the last row of A).
    status |= gr_poly_set(res, tmp_poly, mat_ctx);

    gr_poly_clear(tmp_poly, mat_ctx);

    return status;
}

int _gr_mat_poly_newton_iteration_start(gr_poly_t Y, gr_poly_t Z, gr_poly_t A_denominator_inv, gr_poly_t A_numerator, gr_poly_t A_denominator, gr_mat_t Y0, slong len, int method_flags, gr_ctx_t A_mat_ctx, gr_ctx_t sol_mat_ctx)
{
    gr_ctx_struct *sol_coeff_ctx = MATRIX_CTX(sol_mat_ctx)->base_ring;

    int status = GR_SUCCESS;

    // Initialize A_denominator_inv
    if (method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) {
        // Invert the polynomial A_denominator as a power series.
        status |= gr_poly_inv_series(A_denominator_inv, A_denominator, len, sol_coeff_ctx);
    } else {
        // Calculate the first term of the power series inverse: 1 / A_denominator(0).
        status |= gr_poly_one(A_denominator_inv, sol_coeff_ctx);
        if (gr_poly_length(A_denominator, sol_coeff_ctx) > 0) {
            gr_ptr A_denominator0 = gr_poly_entry_ptr(A_denominator, 0, sol_coeff_ctx);
            status |= gr_poly_div_scalar(A_denominator_inv, A_denominator_inv, A_denominator0, sol_coeff_ctx);
        } else {
            status = GR_DOMAIN;
        }
    }

    if (status != GR_SUCCESS) {
        return status;
    }

    gr_poly_t tmp_poly;
    gr_poly_init(tmp_poly, sol_coeff_ctx);

    gr_mat_t tmp_mat;
    gr_mat_init(tmp_mat, Y0->r, Y0->c, sol_coeff_ctx);

    gr_poly_t tmp_mat_poly;
    gr_poly_init(tmp_mat_poly, sol_mat_ctx);

    // Next we are going to set Y = (I + A(0)*z)*Y0 in a few steps.

    // Y = A(0)*z
    // TODO: Add constant coefficient of A to Y instead of setting and truncating.
    if (A_mat_ctx == sol_mat_ctx) {
        status |= gr_poly_set(Y, A_numerator, sol_mat_ctx);
    } else {
        status |= gr_poly_set_gr_poly_other(Y, A_numerator, A_mat_ctx, sol_mat_ctx);
    }
    status |= gr_poly_truncate(Y, Y, 1, sol_mat_ctx);
    status |= gr_mat_poly_mullow_poly(Y, Y, A_denominator_inv, 1, sol_mat_ctx);
    status |= gr_poly_shift_left(Y, Y, 1, sol_mat_ctx);

    // Y = (I + Y)*Y0
    status |= gr_poly_add_si(Y, Y, 1, sol_mat_ctx);
    status |= gr_poly_mul_scalar(Y, Y, Y0, sol_mat_ctx);

    // Z = Y0^{-1}
    status |= gr_poly_set_scalar(Z, Y0, sol_mat_ctx);
    // TODO: Check Y0 is not zero.
    gr_ptr Z0 = gr_poly_entry_ptr(Z, 0, sol_mat_ctx);
    status |= gr_mat_inv(Z0, Z0, sol_coeff_ctx);

    if (method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) {
        // TODO: Don't modify the input?
        // FIXME: This code assumes A_mat_ctx == sol_mat_ctx

        // Replace A_numerator by the power series expansion of A_numerator / A_denominator.
        status |= gr_mat_poly_mullow_poly(A_numerator, A_numerator, A_denominator_inv, len, sol_mat_ctx);
        // Replace A_denominator by one.
        status |= gr_poly_one(A_denominator, sol_coeff_ctx);
        status |= gr_poly_one(A_denominator_inv, sol_coeff_ctx);
    } else {
        // One initial iteration for A_denominator_inv mod z^2.
        int m = 2;
        status |= gr_poly_truncate(tmp_poly, A_denominator, m, sol_coeff_ctx);
        status |= gr_poly_mullow(tmp_poly, tmp_poly, A_denominator_inv, m, sol_coeff_ctx);
        status |= gr_poly_shift_right(tmp_poly, tmp_poly, m / 2, sol_coeff_ctx);
        status |= gr_poly_mullow(tmp_poly, tmp_poly, A_denominator_inv, m / 2, sol_coeff_ctx);
        status |= gr_poly_shift_left(tmp_poly, tmp_poly, m / 2, sol_coeff_ctx);
        status |= gr_poly_sub(A_denominator_inv, A_denominator_inv, tmp_poly, sol_coeff_ctx);
    }

    gr_poly_clear(tmp_mat_poly, sol_mat_ctx);
    gr_mat_clear(tmp_mat, sol_coeff_ctx);
    gr_poly_clear(tmp_poly, sol_coeff_ctx);

    return status;
}

int _gr_mat_poly_newton_iteration_step(gr_poly_t Y, gr_poly_t Z, gr_poly_t A_denominator_inv, slong *len, gr_poly_t A_numerator, gr_poly_t A_denominator, int method_flags, gr_ctx_t A_mat_ctx, gr_ctx_t sol_mat_ctx)
{
    gr_ctx_struct *sol_coeff_ctx = MATRIX_CTX(sol_mat_ctx)->base_ring;

    gr_poly_t tmp_poly;
    gr_poly_init(tmp_poly, sol_coeff_ctx);

    gr_poly_t tmp_mat_poly;
    gr_poly_init(tmp_mat_poly, sol_mat_ctx);

    slong m = *len;

    int status = GR_SUCCESS;

    // Z = Z + (Z*(I - Y*Z) mod z^m)
    status |= gr_poly_mullow(tmp_mat_poly, Y, Z, m, sol_mat_ctx);
    status |= gr_poly_sub_si(tmp_mat_poly, tmp_mat_poly, 1, sol_mat_ctx);
    status |= gr_poly_mullow(tmp_mat_poly, Z, tmp_mat_poly, m, sol_mat_ctx);
    status |= gr_poly_sub(Z, Z, tmp_mat_poly, sol_mat_ctx);

    if ((method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) == 0) {
        // Iteration for A_denominator_inv mod z^{2*m}.
        status |= gr_poly_truncate(tmp_poly, A_denominator, 2*m, sol_coeff_ctx);
        // TODO: Use middle product here?
        status |= gr_poly_mullow(tmp_poly, tmp_poly, A_denominator_inv, 2*m, sol_coeff_ctx);
        status |= gr_poly_shift_right(tmp_poly, tmp_poly, m, sol_coeff_ctx);
        status |= gr_poly_mullow(tmp_poly, tmp_poly, A_denominator_inv, m, sol_coeff_ctx);
        status |= gr_poly_shift_left(tmp_poly, tmp_poly, m, sol_coeff_ctx);
        status |= gr_poly_sub(A_denominator_inv, A_denominator_inv, tmp_poly, sol_coeff_ctx);
    }

    // Next we are going to set d = Y' - (A mod z^{2m - 1})*Y mod z^(2*m - 1) in a few steps.

    // tmp_mat_poly = (A mod z^{2m - 1})*Y
    // TODO: Optimize truncation of A.
    if (A_mat_ctx == sol_mat_ctx) {
        status |= gr_poly_set(tmp_mat_poly, A_numerator, sol_mat_ctx);
    } else {
        // TODO: Don't redo the same conversion all the time.
        status |= gr_poly_set_gr_poly_other(tmp_mat_poly, A_numerator, A_mat_ctx, sol_mat_ctx);
    }
    if (method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) {
        // TODO: If A_numerator is the full series then the gr_poly_set above is probably excessive in most cases.
        status |= gr_poly_truncate(tmp_mat_poly, tmp_mat_poly, 2*m - 1, sol_mat_ctx);
    } else {
        status |= gr_mat_poly_mullow_poly(tmp_mat_poly, tmp_mat_poly, A_denominator_inv, 2*m - 1, sol_mat_ctx);
    }

    if (method_flags & SOLVE_ODE_METHOD_ASSUME_A_IS_COMPANION) {
        status |= gr_mat_poly_mullow_companion(tmp_mat_poly, tmp_mat_poly, Y, 2*m - 1, sol_mat_ctx);
    } else {
        status |= gr_poly_mullow(tmp_mat_poly, tmp_mat_poly, Y, 2*m - 1, sol_mat_ctx);
    }

    // d = Y' - tmp_mat_poly
    gr_poly_t d;
    gr_poly_init(d, sol_mat_ctx);
    status |= gr_poly_derivative(d, Y, sol_mat_ctx);
    status |= gr_poly_sub(d, d, tmp_mat_poly, sol_mat_ctx);

    // Y = Y - (Y*integral(Z*d) mod z^{2m})
    status |= gr_poly_mullow(tmp_mat_poly, Z, d, 2*m - 1, sol_mat_ctx);
    status |= gr_poly_integral(tmp_mat_poly, tmp_mat_poly, sol_mat_ctx);
    status |= gr_poly_mullow(tmp_mat_poly, Y, tmp_mat_poly, 2*m, sol_mat_ctx);
    status |= gr_poly_sub(Y, Y, tmp_mat_poly, sol_mat_ctx);

    *len *= 2;

    gr_poly_clear(d, sol_mat_ctx);
    gr_poly_clear(tmp_mat_poly, sol_mat_ctx);
    gr_poly_clear(tmp_poly, sol_coeff_ctx);

    return status;
}

int gr_mat_poly_solve_ode_newton(gr_poly_t Y, gr_poly_t A_numerator, gr_poly_t A_denominator, gr_mat_t Y0, slong len, int method_flags, gr_ctx_t A_mat_ctx, gr_ctx_t sol_mat_ctx)
{
    gr_ctx_struct *A_coeff_ctx = MATRIX_CTX(A_mat_ctx)->base_ring;
    gr_ctx_struct *sol_coeff_ctx = MATRIX_CTX(sol_mat_ctx)->base_ring;

    int status = GR_SUCCESS;

    // Convert A_denominator into the solution context.
    gr_poly_t A_denominator_sol;
    gr_poly_init(A_denominator_sol, sol_coeff_ctx);
    if (A_mat_ctx == sol_mat_ctx) {
        status |= gr_poly_set(A_denominator_sol, A_denominator, sol_coeff_ctx);
    } else {
        status |= gr_poly_set_gr_poly_other(A_denominator_sol, A_denominator, A_coeff_ctx, sol_coeff_ctx);
    }

    // TODO: Don't modify the input?
    if (method_flags & SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES) {
        status |= gr_poly_one(A_denominator, A_coeff_ctx);
    }

    // The power series expansion of the inverse of A_denominator.
    gr_poly_t A_denominator_inv;
    gr_poly_init(A_denominator_inv, sol_coeff_ctx);

    // The power series expansion of the inverse matrix Z of the matrix Y.
    gr_poly_t Z;
    gr_poly_init(Z, sol_mat_ctx);

    slong m = 2;
    status |= _gr_mat_poly_newton_iteration_start(Y, Z, A_denominator_inv, A_numerator, A_denominator_sol, Y0, len, method_flags, A_mat_ctx, sol_mat_ctx);
    while (m <= len / 2) {
        status |= _gr_mat_poly_newton_iteration_step(Y, Z, A_denominator_inv, &m, A_numerator, A_denominator_sol, method_flags, A_mat_ctx, sol_mat_ctx);
    }

    // TODO: Add a way to retrieve the inverse Z.
    gr_poly_clear(Z, sol_mat_ctx);
    gr_poly_clear(A_denominator_inv, sol_coeff_ctx);
    gr_poly_clear(A_denominator_sol, sol_coeff_ctx);

    return status;
}
