# Improved implementation using a single vector instead of a dictionary.
macro binary_splitting(expr::Expr)
    @assert expr.head == :call
    @assert expr.args[1] == :*
    factors = expr.args[2:end]
    num_factors = length(factors)
    @assert ispow2(num_factors)
    exponent = Integer(log2(num_factors))
    A = Vector{Any}(undef, num_factors) # TODO: Can use Expr values?
    # Initialization.
    for K in 1:num_factors
        A[K] = factors[K]
    end
    # Chudnovsky & Chudnovsky, p. 116. Simplified using in-place update.
    num_factors_divided_by_power_of_two = num_factors >> 1
    for ell in 1:exponent
        for K in 1:num_factors_divided_by_power_of_two
            A[K] = Expr(:call, :*, A[2*K - 1], A[2*K])
        end
        num_factors_divided_by_power_of_two >>= 1
    end
    return quote $A[1] end
end
