#include "differential_operator_flint.h"

int gr_poly_mat_from_scalar_diffop(gr_mat_t A_numerator, gr_poly_t A_denominator, gr_poly_t A_scalarform, gr_ctx_t poly_ctx)
{
    int status = GR_SUCCESS;

    gr_ctx_struct *coeff_ctx = POLYNOMIAL_CTX(poly_ctx)->base_ring;

    slong len = gr_poly_length(A_scalarform, poly_ctx);

    slong diff_order = len - 1;

    if (len > 0) {
        status |= gr_poly_set(A_denominator, gr_poly_entry_ptr(A_scalarform, diff_order, poly_ctx), coeff_ctx);
    } else {
        status |= gr_poly_one(A_denominator, coeff_ctx);
    }

    status |= gr_mat_zero(A_numerator, poly_ctx);

    if (len == 0) {
        return status;
    }

    for (int i = 0; i < diff_order - 1; i++) {
        status |= gr_poly_set(gr_mat_entry_ptr(A_numerator, i, i + 1, poly_ctx), A_denominator, coeff_ctx);
    }

    for (int i = 0; i < diff_order; i++) {
        gr_ptr A_numerator_entry = gr_mat_entry_ptr(A_numerator, diff_order - 1, i, poly_ctx);
        gr_ptr A_scalarform_entry = gr_poly_entry_ptr(A_scalarform, i, poly_ctx);
        status |= gr_poly_sub(A_numerator_entry, A_numerator_entry, A_scalarform_entry, coeff_ctx);
    }

    return status;
}

int gr_mat_poly_from_scalar_diffop(gr_poly_t A_numerator, gr_poly_t A_denominator, gr_poly_t A_scalarform, gr_ctx_t mat_ctx, gr_ctx_t poly_ctx)
{
    int status = GR_SUCCESS;

    gr_ctx_struct *coeff_ctx = POLYNOMIAL_CTX(poly_ctx)->base_ring;

    slong len = gr_poly_length(A_scalarform, poly_ctx);
    slong diff_order = len - 1;

    if (len > 0) {
        status |= gr_poly_set(A_denominator, gr_poly_entry_ptr(A_scalarform, diff_order, poly_ctx), coeff_ctx);
    } else {
        status |= gr_poly_one(A_denominator, coeff_ctx);
    }

    slong degree = -1;
    for (int i = 0; i < len; i++) {
        slong coeff_degree = gr_poly_length(gr_poly_entry_ptr(A_scalarform, i, poly_ctx), coeff_ctx) - 1;
        if (coeff_degree > degree) {
            degree = coeff_degree;
        }
    }
    gr_poly_fit_length(A_numerator, degree + 1, mat_ctx);
    _gr_poly_set_length(A_numerator, degree + 1, mat_ctx);

    if (len == 0) {
        return status;
    }

    slong A_denominator_degree = gr_poly_length(A_denominator, coeff_ctx) - 1;
    for (int i = 0; i <= A_denominator_degree; i++) {
        gr_ptr A_denominator_coeff = gr_poly_entry_ptr(A_denominator, i, coeff_ctx);
        gr_mat_struct *A_numerator_coeff = (gr_mat_struct*)gr_poly_entry_ptr(A_numerator, i, mat_ctx);
        for (int j = 0; j < diff_order - 1; j++) {
            gr_ptr A_numerator_coeff_entry = gr_mat_entry_ptr(A_numerator_coeff, j, j + 1, coeff_ctx);
            status |= gr_set(A_numerator_coeff_entry, A_denominator_coeff, coeff_ctx);
        }
    }

    for (int j = 0; j < diff_order; j++) {
        gr_ptr A_scalarform_coeff = gr_poly_entry_ptr(A_scalarform, j, poly_ctx);
        for (int d = 0; d < gr_poly_length(A_scalarform_coeff, coeff_ctx); d++) {
            gr_mat_struct *A_numerator_coeff = (gr_mat_struct*)gr_poly_entry_ptr(A_numerator, d, mat_ctx);
            gr_ptr A_numerator_coeff_entry = gr_mat_entry_ptr(A_numerator_coeff, diff_order - 1, j, coeff_ctx);
            gr_ptr A_scalarform_coeff_entry = gr_poly_entry_ptr(A_scalarform_coeff, d, coeff_ctx);
            status |= gr_sub(A_numerator_coeff_entry, A_numerator_coeff_entry, A_scalarform_coeff_entry, coeff_ctx);
        }
    }

    return status;
}

/*
 * This is an implementation of the cyclic vector lemma.
 * Fraction-free version of the algorithm communicated by Marc Mezzarobba.
 */

int scalar_diffop_from_gr_poly_mat(gr_poly_t A_scalarform, gr_mat_t A_numerator, gr_poly_t A_denominator, gr_ctx_t poly_ctx) {
    int status = GR_SUCCESS;

    gr_ctx_struct *coeff_ctx = POLYNOMIAL_CTX(poly_ctx)->base_ring;

    gr_poly_t A_denominator_derivative;
    gr_poly_init(A_denominator_derivative, coeff_ctx);
    status |= gr_poly_derivative(A_denominator_derivative, A_denominator, coeff_ctx);

    gr_mat_t tmp_row;
    gr_mat_init(tmp_row, 1, A_numerator->c, poly_ctx);

    gr_poly_t tmp_poly;
    gr_poly_init(tmp_poly, coeff_ctx);

    gr_mat_t P;
    gr_mat_init(P, A_numerator->r + 1, A_numerator->c, poly_ctx);

    // Let ∇(v) = dv/dx + v*A(x).
    // Find a vector u such that P = [u; ∇u; ...; ∇ⁿ⁻¹u] is invertible.

    flint_rand_t state;
    flint_rand_init(state);

    int cyclic_vector_found = 0;
    // Always start by trying u = (1, 0, 0, 0) as the first row.
    status |= gr_one(gr_mat_entry_ptr(P, 0, 0, poly_ctx), poly_ctx);
    for (slong j = 1; j < P->c; j++) {
        status |= gr_zero(gr_mat_entry_ptr(P, 0, j, poly_ctx), poly_ctx);
    }
    do {
        // Get each next row by applying
        //     ∇(v) = A_denominator(x)*dv/dx + v*A_numerator(x) - (i-1)*A_denominator'(x)*v
        // to the previous row.
        for (slong i = 1; i < P->r; i++) {
            for (slong j = 0; j < P->c; j++) {
                gr_ptr entry_P = gr_mat_entry_ptr(P, i, j, poly_ctx);
                gr_ptr entry_P_above = gr_mat_entry_ptr(P, i - 1, j, poly_ctx);

                // A_denominator(x)*dv/dx
                status |= gr_poly_derivative(entry_P, entry_P_above, coeff_ctx);
                status |= gr_poly_mul(entry_P, entry_P, A_denominator, coeff_ctx);

                for (slong k = 0; k < A_numerator->r; k++) {
                    // v*A_numerator(x)
                    gr_ptr entry_A = gr_mat_entry_ptr(A_numerator, k, j, poly_ctx);
                    gr_ptr entry_P_prev = gr_mat_entry_ptr(P, i - 1, k, poly_ctx);
                    status |= gr_addmul(entry_P, entry_P_prev, entry_A, poly_ctx);
                }

                // - (i-1)*A_denominator'(x)*v
                if (i > 1) {
                    status |= gr_poly_mul(tmp_poly, A_denominator_derivative, entry_P_above, coeff_ctx);
                    status |= gr_submul_si(entry_P, tmp_poly, i - 1, poly_ctx);
                }
            }
        }

        // Rescale rows by powers of A_denominator.
        status |= gr_poly_set(tmp_poly, A_denominator, coeff_ctx);
        for (slong i = P->r - 2; i >= 0; i--) {
            for (slong j = 0; j < P->c; j++) {
                gr_ptr entry_P = gr_mat_entry_ptr(P, i, j, poly_ctx);
                status |= gr_poly_mul(entry_P, entry_P, tmp_poly, coeff_ctx);
            }
            status |= gr_poly_mul(tmp_poly, tmp_poly, A_denominator, coeff_ctx);
        }

        slong rank_P;
        status |= gr_mat_rank(&rank_P, P, poly_ctx);
        cyclic_vector_found = rank_P == A_numerator->r;
        if (cyclic_vector_found == 0) {
            // Choose a random vector u.
            for (slong j = 0; j < A_numerator->c; j++) {
                status |= gr_randtest_small(gr_mat_entry_ptr(P, 0, j, poly_ctx), state, poly_ctx);
            }
        }
    } while (cyclic_vector_found == 0);

    flint_rand_clear(state);

    // Find a vector b in the left kernel of P, i.e. such that b*P = 0.

    // TODO: Avoid transpose.
    // NOTE: Aliasing in gr_mat_transpose is only allowed for square matrices.
    gr_mat_t P_transposed;
    gr_mat_init(P_transposed, P->c, P->r, poly_ctx);
    status |= gr_mat_transpose(P_transposed, P, poly_ctx);

    gr_mat_t b;
    gr_mat_init(b, P_transposed->r, 1, poly_ctx);

    // TODO: Use null space algorithm specific for univariate polynomial rings.
    status |= gr_mat_nullspace(b, P_transposed, poly_ctx);

    // b = (b₁, ..., bₙ) are the coefficients of a scalar equation equivalent to dY/dx = A(x)*Y

    if (b->c > 0) {
        if (gr_ctx_is_exact(coeff_ctx) == T_TRUE) {
            // Compute the GCD of the entries of b.
            status |= gr_poly_zero(tmp_poly, coeff_ctx);
            for (slong i = 0; i < b->r; i++) {
                status |= gr_poly_gcd(tmp_poly, tmp_poly, gr_mat_entry_ptr(b, i, 0, poly_ctx), coeff_ctx);
            }

            // Divide the entries of b by their GCD.
            for (slong i = 0; i < b->r; i++) {
                gr_poly_struct *entry_b = (gr_poly_struct*)gr_mat_entry_ptr(b, i, 0, poly_ctx);
                status |= gr_poly_div(entry_b, entry_b, tmp_poly, coeff_ctx);
            }

            // TODO: Also divide out a common constant factor?
        }

        gr_poly_fit_length(A_scalarform, b->r, coeff_ctx);
        _gr_poly_set_length(A_scalarform, b->r, coeff_ctx);
        for (slong i = 0; i < b->r; i++) {
            status |= gr_poly_set(gr_poly_entry_ptr(A_scalarform, i, poly_ctx), gr_mat_entry_ptr(b, i, 0, poly_ctx), coeff_ctx);
        }
    } else {
        status |= gr_poly_zero(A_scalarform, poly_ctx);
        status |= GR_UNABLE;
    }

    gr_mat_clear(b, poly_ctx);
    gr_mat_clear(P_transposed, poly_ctx);
    gr_mat_clear(P, poly_ctx);
    gr_poly_clear(tmp_poly, coeff_ctx);
    gr_mat_clear(tmp_row, poly_ctx);
    gr_poly_clear(A_denominator_derivative, coeff_ctx);

    return status;
}
