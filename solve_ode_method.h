#ifndef SOLVE_ODE_METHOD_H
#define SOLVE_ODE_METHOD_H

typedef int (*gr_poly_mat_solve_ode_method_t)(gr_mat_t Y, gr_mat_t A_numerator, gr_poly_t A_denominator, gr_mat_t Y0, slong len, int method_flags, gr_ctx_t A_poly_ctx, gr_ctx_t sol_poly_ctx);
typedef int (*gr_mat_poly_solve_ode_method_t)(gr_poly_t Y, gr_poly_t A_numerator, gr_poly_t A_denominator, gr_mat_t Y0, slong len, int method_flags, gr_ctx_t A_mat_ctx, gr_ctx_t sol_mat_ctx);

enum solve_ode_method_flags {
    SOLVE_ODE_METHOD_ASSUME_A_IS_COMPANION = 1 << 0,
    SOLVE_ODE_METHOD_PRE_COMPUTE_A_SERIES = 1 << 1,
};
#define SOLVE_ODE_METHOD_FLAG_COUNT 2
#define SOLVE_ODE_METHOD_FLAG_MAX (1 << SOLVE_ODE_METHOD_FLAG_COUNT)

#endif // SOLVE_ODE_METHOD_H
